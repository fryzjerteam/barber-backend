package barber.aws;

import barber.errorhandling.httpstatus.CorruptDataException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.usermanagement.dto.BasicUserDto;
import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.dto.UserRegisterDataDts;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.*;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.annotation.Nonnull;

@Service
public class AwsEmailServiceImpl implements AwsEmailService {

    public static final String REGISTRATION_SUBJECT = "Welcome in Barber.PL !";
    public static final String REMINDER_SUBJECT = "Barber.PL Appointment reminder";

    @Value("${aws.source.email}")
    private String sourceEmail;

    @Autowired
    TemplateEngine templateEngine;

    @Override
    public void sendRegistrationEmail(@Nonnull UserRegisterDataDts userRegisterDataDts) {
        Preconditions.checkNotNull(userRegisterDataDts);

        BasicUserDto basicUserDto = userRegisterDataDts.getBasicUserDto();
        UserDetailsDto userDetailsDto = userRegisterDataDts.getUserDetailsDto();
        if(basicUserDto != null && userDetailsDto != null){

            Context context = new Context();
            context.setVariable("firstName", basicUserDto.getFirstName());
            context.setVariable("login", basicUserDto.getName());
            context.setVariable("password", basicUserDto.getPassword());
            String body = templateEngine.process("registrationEmail", context);
            sendEmail(userDetailsDto.getEmail(), REGISTRATION_SUBJECT, body);
            return;
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public void sendReminderEmail(String email, String firstName, String service, String time) {
        Context context = new Context();
        context.setVariable("firstName", firstName);
        context.setVariable("service", service);
        context.setVariable("time", time);
        String body = templateEngine.process("reminderEmail", context);
        sendEmail(email, REMINDER_SUBJECT, body);
    }

    private void sendEmail(String email, String subject, String body){
        Preconditions.checkNotNull(email);
        Preconditions.checkNotNull(subject);
        Preconditions.checkNotNull(body);
        try {
            AmazonSimpleEmailService client =
                    AmazonSimpleEmailServiceClientBuilder.standard()
                            // Replace US_WEST_2 with the AWS Region you're using for
                            // Amazon SES.
                            .withRegion(Regions.EU_WEST_1).build();
            SendEmailRequest request = new SendEmailRequest()
                    .withDestination(
                            new Destination().withToAddresses(email))
                    .withMessage(new Message()
                            .withBody(new Body()
                                    .withHtml(new Content()
                                            .withCharset("UTF-8").withData(body)))
                            .withSubject(new Content()
                                    .withCharset("UTF-8").withData(subject)))
                    .withSource(sourceEmail);
            client.sendEmail(request);
        } catch (Exception ex) {
            System.out.println("The templates was not sent. Error message: "
                    + ex.getMessage());
            throw new CorruptDataException();
        }
    }
}
