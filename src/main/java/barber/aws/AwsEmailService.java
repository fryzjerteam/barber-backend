package barber.aws;

import barber.usermanagement.dto.UserRegisterDataDts;

import javax.annotation.Nonnull;

public interface AwsEmailService {
    void sendRegistrationEmail(@Nonnull UserRegisterDataDts userRegisterDataDts);

    void sendReminderEmail(String email, String firstName, String service, String time);
}
