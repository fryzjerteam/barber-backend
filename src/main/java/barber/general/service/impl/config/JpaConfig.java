package barber.general.service.impl.config;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableJpaRepositories
public class JpaConfig {
}
