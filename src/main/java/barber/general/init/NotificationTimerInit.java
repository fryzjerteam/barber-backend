package barber.general.init;

import barber.aws.AwsEmailService;
import barber.bookingmanagement.dto.BookingDto;
import barber.bookingmanagement.dto.SearchBookingsDts;
import barber.bookingmanagement.dto.localdatetime.LocalDateDto;
import barber.bookingmanagement.service.BookingService;
import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Component
public class NotificationTimerInit {

    @Autowired
    AwsEmailService awsEmailService;
    @Autowired
    BookingService bookingService;
    @Autowired
    UserDetailsService userDetailsService;

    public void init() throws InterruptedException {
        TimerTask repeatedTask = new TimerTask() {
            public void run() {

                SearchBookingsDts searchBookingDts = new SearchBookingsDts();
                LocalDateDto date = new LocalDateDto();
                date.setDay(LocalDate.now().plusDays(1).getDayOfMonth());
                date.setMonth(LocalDate.now().plusDays(1).getMonthValue());
                date.setYear(LocalDate.now().plusDays(1).getYear());
                searchBookingDts.setStartDate(date);
                searchBookingDts.setEndDate(date);
                List<BookingDto> bookingsByDay = bookingService.findBookingsByDay(searchBookingDts);

                for (BookingDto booking : bookingsByDay) {
                    String firstName = booking.getClient().getFirstName();
                    String service = booking.getServiceAssignmentDto().getServiceDto().getName();
                    int hour = booking.getStartDateTimeDto().getLocalTimeDto().getHour();
                    int minute = booking.getStartDateTimeDto().getLocalTimeDto().getMinute();
                    String minuteString = minute < 10 ? "0" + Integer.toString(minute) : Integer.toString(minute);
                    String time = Integer.toString(hour) + ":" + minuteString;
                    UserDetailsDto userDetails = userDetailsService.findUserDetailsByUserId(booking.getClient().getId());
                    if(userDetails != null){
                        String email = userDetails.getEmail();
                        awsEmailService.sendReminderEmail(email, firstName, service, time);
                    }
                }
            }
        };
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        LocalDateTime tomorrowAt12 = LocalDateTime.of(LocalDate.now().plusDays(1), LocalTime.of(12, 00));
        LocalDateTime now = LocalDateTime.now();
        long delay = Duration.between(now, tomorrowAt12).toMillis();
        long period = 1000L * 60L * 60L * 24L;
        executor.scheduleAtFixedRate(repeatedTask, delay, period, TimeUnit.MILLISECONDS);
    }
}
