package barber.general.init.config;

import barber.general.init.FirstUserInit;
import barber.general.init.NotificationTimerInit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InitClassConfig {

    @Bean(initMethod="init")
    public NotificationTimerInit notificationTimerInit(){
        return new NotificationTimerInit();
    }

    @Bean(initMethod="init")
    public FirstUserInit firstUserInit(){
        return new FirstUserInit();
    }
}
