package barber.general.init;

import barber.general.common.api.datatype.Role;
import barber.usermanagement.dto.BasicUserDto;
import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.dto.UserRegisterDataDts;
import barber.usermanagement.service.UserRegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;


@Component
public class FirstUserInit {

    @Autowired
    UserRegistrationService registrationService;

    @Value("${first.user.login}")
    private String login;

    @Value("${first.user.password}")
    private String password;

    @Value("${first.user.email}")
    private String email;


    public void init() throws InterruptedException {
        if(this.login != null && this.password != null){
            UserRegisterDataDts data = new UserRegisterDataDts();
            BasicUserDto user = createUser();
            data.setBasicUserDto(user);
            UserDetailsDto details = createDetails();
            data.setUserDetailsDto(details);
            registrationService.registerUser(data);
        }
    }

    private UserDetailsDto createDetails() {
        UserDetailsDto details = new UserDetailsDto();
        details.setEmail(this.email);
        details.setPhone("123123123");
        return details;
    }

    private BasicUserDto createUser() {
        BasicUserDto user = new BasicUserDto();
        user.setFirstName("Admin");
        user.setLastName("Admin");
        user.setName(this.login);
        user.setPassword(this.password);
        user.setRole(Role.ADMIN);
        return user;
    }
}
