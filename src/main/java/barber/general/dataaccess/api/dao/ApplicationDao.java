package barber.general.dataaccess.api.dao;

import io.oasp.module.jpa.dataaccess.api.GenericRevisionedDao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

import java.util.List;

/**
 * Interface for all {@link GenericRevisionedDao DAOs} (Data Access Object) of this application.
 *
 *
 * @param <ENTITY> is the type of the managed entity.
 */
@NoRepositoryBean
public interface ApplicationDao<ENTITY> extends CrudRepository<ENTITY, Long> {
    List<ENTITY> findAll();
}
