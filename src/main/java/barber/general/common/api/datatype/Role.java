package barber.general.common.api.datatype;

import java.security.Principal;


public enum Role implements Principal {

  // BEGIN ARCHETYPE SKIP
  /**
   * AccessControlGroup of a cook who works in the kitchen and can see the orders with their positions. He prepares the
   * menus and side-dishes and can mark order-positions as prepared.
   */
  ADMIN("Admin"),

  CLIENT("Client"),

  EMPLOYEE("Employee");

  private final String name;

  private Role(String name) {

    this.name = name;
  }

  @Override
  public String getName() {

    return this.name;
  }
}
