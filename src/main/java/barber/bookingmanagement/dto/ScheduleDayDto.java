package barber.bookingmanagement.dto;

import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;

public class ScheduleDayDto {
    private LocalTimeDto startTime;
    private LocalTimeDto endTime;
    private String dayName;


    public LocalTimeDto getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTimeDto startTime) {
        this.startTime = startTime;
    }

    public LocalTimeDto getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTimeDto endTime) {
        this.endTime = endTime;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }


}

