package barber.bookingmanagement.dto;


import barber.bookingmanagement.dto.localdatetime.LocalDateTimeDto;
import barber.bookingmanagement.common.BookingState;
import barber.general.common.api.to.UserDto;

public class BookingDto {
    private Long id;
    private ServiceAssignmentDto serviceAssignmentDto;
    private LocalDateTimeDto startDateTimeDto;
    private UserDto client;
    private BookingState bookingState;

    public ServiceAssignmentDto getServiceAssignmentDto() {
        return serviceAssignmentDto;
    }

    public void setServiceAssignmentDto(ServiceAssignmentDto serviceAssignmentDto) {
        this.serviceAssignmentDto = serviceAssignmentDto;
    }

    public LocalDateTimeDto getStartDateTimeDto() {
        return startDateTimeDto;
    }

    public void setStartDateTimeDto(LocalDateTimeDto startDateTimeDto) {
        this.startDateTimeDto = startDateTimeDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public UserDto getClient() {
        return client;
    }

    public void setClient(UserDto client) {
        this.client = client;
    }

    public BookingState getBookingState() {
        return bookingState;
    }

    public void setBookingState(BookingState bookingState) {
        this.bookingState = bookingState;
    }
}
