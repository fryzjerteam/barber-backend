package barber.bookingmanagement.dto.localdatetime;

import io.oasp.module.basic.common.api.to.AbstractTo;

public class LocalDateTimeDto extends AbstractTo {
    private LocalDateDto localDateDto;
    private LocalTimeDto localTimeDto;

    public LocalDateDto getLocalDateDto() {
        return localDateDto;
    }

    public void setLocalDateDto(LocalDateDto localDateDto) {
        this.localDateDto = localDateDto;
    }

    public LocalTimeDto getLocalTimeDto() {
        return localTimeDto;
    }

    public void setLocalTimeDto(LocalTimeDto localTimeDto) {
        this.localTimeDto = localTimeDto;
    }
}
