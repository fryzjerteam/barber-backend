package barber.bookingmanagement.dto.localdatetime;

import io.oasp.module.basic.common.api.to.AbstractTo;

public class LocalTimeDto extends AbstractTo {

    private int hour;
    private int minute;
    private int second;

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }
}
