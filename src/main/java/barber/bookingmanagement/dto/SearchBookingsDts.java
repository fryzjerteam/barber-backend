package barber.bookingmanagement.dto;

import barber.bookingmanagement.dto.localdatetime.LocalDateDto;
import barber.general.common.api.to.UserDto;
import dz.jtsgen.annotations.TypeScript;
import io.oasp.module.basic.common.api.to.AbstractTo;

@TypeScript
public class SearchBookingsDts extends AbstractTo{

    private UserDto userDto;
    private ServiceDto serviceDto;
    private LocalDateDto startDate;
    private LocalDateDto endDate;

    public LocalDateDto getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDateDto startDate) {
        this.startDate = startDate;
    }

    public LocalDateDto getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDateDto endDate) {
        this.endDate = endDate;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public ServiceDto getServiceDto() {
        return serviceDto;
    }

    public void setServiceDto(ServiceDto serviceDto) {
        this.serviceDto = serviceDto;
    }
}
