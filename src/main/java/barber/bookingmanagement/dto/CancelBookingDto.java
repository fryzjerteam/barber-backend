package barber.bookingmanagement.dto;

import io.oasp.module.basic.common.api.to.AbstractTo;

public class CancelBookingDto extends AbstractTo {
    private Long bookingId;
    private String note;

    public Long getBookingId() {
        return bookingId;
    }

    public void setBookingId(Long bookingId) {
        this.bookingId = bookingId;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
