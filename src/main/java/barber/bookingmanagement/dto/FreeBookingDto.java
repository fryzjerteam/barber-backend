package barber.bookingmanagement.dto;

import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;
import dz.jtsgen.annotations.TypeScript;
import io.oasp.module.basic.common.api.to.AbstractTo;

import java.util.List;

@TypeScript
public class FreeBookingDto extends AbstractTo{
    private List<LocalTimeDto> startTimes;
    private ServiceAssignmentDto serviceAssignmentDto;


    public ServiceAssignmentDto getServiceAssignmentDto() {
        return serviceAssignmentDto;
    }

    public void setServiceAssignmentDto(ServiceAssignmentDto serviceAssignmentDto) {
        this.serviceAssignmentDto = serviceAssignmentDto;
    }

    public List<LocalTimeDto> getStartTimes() {
        return startTimes;
    }

    public void setStartTimes(List<LocalTimeDto> startTimes) {
        this.startTimes = startTimes;
    }
}
