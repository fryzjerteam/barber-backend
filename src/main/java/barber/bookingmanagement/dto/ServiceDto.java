package barber.bookingmanagement.dto;

import io.oasp.module.basic.common.api.to.AbstractTo;

public class ServiceDto extends AbstractTo {
    private Long id;
    private String name;
    private Integer durationUnits;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDurationUnits() {
        return durationUnits;
    }

    public void setDurationUnits(Integer durationUnits) {
        this.durationUnits = durationUnits;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
