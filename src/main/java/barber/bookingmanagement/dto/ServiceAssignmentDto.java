package barber.bookingmanagement.dto;

import barber.general.common.api.to.UserDto;
import io.oasp.module.basic.common.api.to.AbstractTo;

public class ServiceAssignmentDto extends AbstractTo {
    private Long id;
    private ServiceDto serviceDto;
    private UserDto userDto;

    public ServiceDto getServiceDto() {
        return serviceDto;
    }

    public void setServiceDto(ServiceDto serviceDto) {
        this.serviceDto = serviceDto;
    }

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
