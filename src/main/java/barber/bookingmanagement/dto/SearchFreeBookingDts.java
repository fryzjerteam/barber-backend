package barber.bookingmanagement.dto;

import barber.bookingmanagement.dto.localdatetime.LocalDateDto;
import dz.jtsgen.annotations.TypeScript;
import io.oasp.module.basic.common.api.to.AbstractTo;

@TypeScript
public class SearchFreeBookingDts extends AbstractTo{
    private ServiceDto serviceDto;
    private LocalDateDto localDateDto;

    public ServiceDto getServiceDto() {
        return serviceDto;
    }

    public void setServiceDto(ServiceDto serviceDto) {
        this.serviceDto = serviceDto;
    }

    public LocalDateDto getLocalDateDto() {
        return localDateDto;
    }

    public void setLocalDateDto(LocalDateDto localDateDto) {
        this.localDateDto = localDateDto;
    }
}
