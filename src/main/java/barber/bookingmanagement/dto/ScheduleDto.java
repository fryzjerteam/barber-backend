package barber.bookingmanagement.dto;

import barber.bookingmanagement.entity.ScheduleDay;
import barber.general.common.api.to.UserDto;
import io.oasp.module.basic.common.api.to.AbstractTo;

import java.util.List;

public class ScheduleDto extends AbstractTo {

    private Long id;
    private UserDto userDto;
    private List<ScheduleDayDto> days;

    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<ScheduleDayDto> getDays() {
        return days;
    }

    public void setDays(List<ScheduleDayDto> days) {
        this.days = days;
    }
}
