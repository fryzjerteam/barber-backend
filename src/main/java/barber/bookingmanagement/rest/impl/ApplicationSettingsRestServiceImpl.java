package barber.bookingmanagement.rest.impl;

import barber.bookingmanagement.rest.api.ApplicationSettingsRestService;
import barber.bookingmanagement.service.ApplicationSettingsService;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.dto.UserDataDts;
import barber.usermanagement.dto.UserRegisterDataDts;
import barber.usermanagement.service.UserRegistrationService;
import barber.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@Named
@Transactional
public class ApplicationSettingsRestServiceImpl implements ApplicationSettingsRestService {

    @Autowired
    ApplicationSettingsService appSettingsService;

    @Override
    public String getSetting(String setting) {
        return appSettingsService.getSetting(setting);
    }

    @Override
    public String updateSetting(String setting, String value) {
        return null;
    }
}
