package barber.bookingmanagement.rest.impl;

import barber.bookingmanagement.dto.ServiceAssignmentDto;
import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.rest.api.ServiceAssignmentRestService;
import barber.bookingmanagement.service.ServiceAssignmentService;
import barber.general.common.api.to.UserDto;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@Named
@Transactional
public class ServiceAssignmentRestServiceImpl implements ServiceAssignmentRestService {

    @Autowired
    ServiceAssignmentService serviceAssignmentService;

    @Override
    public List<ServiceDto> getServicesByUser(UserDto userDto) {
        return serviceAssignmentService.getServicesByUser(userDto);
    }

    @Override
    public ServiceDto addAssignment(ServiceAssignmentDto serviceAssignmentDto) {
        return serviceAssignmentService.addAssignment(serviceAssignmentDto);
    }

    @Override
    public void deleteAssignment(ServiceAssignmentDto serviceAssignmentDto) {
        serviceAssignmentService.deleteAssignment(serviceAssignmentDto);
    }
}
