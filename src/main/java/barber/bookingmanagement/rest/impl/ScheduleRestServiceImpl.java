package barber.bookingmanagement.rest.impl;

import barber.bookingmanagement.dto.ScheduleDto;
import barber.bookingmanagement.rest.api.ScheduleRestService;
import barber.bookingmanagement.service.ScheduleService;
import barber.general.common.api.to.UserDto;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Named
public class ScheduleRestServiceImpl implements ScheduleRestService {

    @Autowired
    ScheduleService scheduleService;

    @Override
    public List<ScheduleDto> findByUser(UserDto userDto) {
        return scheduleService.findScheduleByUser(userDto);
    }

    @Override
    public ScheduleDto addSchedule(ScheduleDto scheduleDto) {
        return scheduleService.addSchedule(scheduleDto);
    }

    @Override
    public ScheduleDto updateSchedule(ScheduleDto scheduleDto) {
        return scheduleService.updateSchedule(scheduleDto);
    }

    @Override
    public void deleteSchedule(String id) {
        scheduleService.deleteSchedule(id);
    }
}
