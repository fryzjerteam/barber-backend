package barber.bookingmanagement.rest.impl;

import barber.bookingmanagement.dto.*;
import barber.bookingmanagement.rest.api.BookingRestService;
import barber.bookingmanagement.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@Named
@Transactional
public class BookingRestServiceImpl implements BookingRestService {

    @Autowired
    BookingService bookingService;

    @Override
    public List<FreeBookingDto> getFreeBookings(SearchFreeBookingDts searchFreeBookingDts) {
        return bookingService.findFreeBookings(searchFreeBookingDts);
    }

    @Override
    public BookingDto createClientBooking(BookingDto bookingDto) {
        return bookingService.createClientBooking(bookingDto);
    }

    @Override
    public List<BookingDto> getBookingsByEmployee(SearchBookingsDts searchBookingsDts) {
        return bookingService.findBookingsByEmployee(searchBookingsDts);
    }

    @Override
    public List<BookingDto> getBookingsByDay(SearchBookingsDts searchBookingsDts) {
        return bookingService.findBookingsByDay(searchBookingsDts);
    }

    @Override
    public List<BookingDto> getBookingsCanceledByDay(SearchBookingsDts searchBookingsDts) {
        return bookingService.findCanceledBookingsByDay(searchBookingsDts);
    }

    @Override
    public List<BookingDto> getBookingsByService(SearchBookingsDts searchBookingsDts) {
        return bookingService.findBookingsByService(searchBookingsDts);
    }

    @Override
    public List<BookingDto> getFutureClientBookings() {
        return bookingService.findFutureClientsBookings();
    }

    @Override
    public List<BookingDto> getOldClientBookings() {
        return bookingService.findOldClientBookings();
    }

    @Override
    public void cancelBooking(CancelBookingDto cancelBookingDto) {
        bookingService.cancelBooking(cancelBookingDto);
    }

    @Override
    public List<BookingDto> getBookingsByCurrentEmployee(SearchBookingsDts searchBookingsDts) {
        return bookingService.findBookingsByCurrentEmployee(searchBookingsDts);
    }


}
