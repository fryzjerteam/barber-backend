package barber.bookingmanagement.rest.impl;

import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.rest.api.ServicesRestService;
import barber.bookingmanagement.service.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@Named
@Transactional
public class ServicesRestServiceImpl implements ServicesRestService {

    @Autowired
    ServicesService servicesService;

    @Override
    public List<ServiceDto> getAllServices() {
        return servicesService.getAllServices();
    }

    @Override
    public List<ServiceDto> addService(ServiceDto serviceDto) {
        return servicesService.addService(serviceDto);
    }

    @Override
    public ServiceDto updateService(ServiceDto serviceDto) {
        return servicesService.updateService(serviceDto);
    }

    @Override
    public void deleteService(String id) {
        servicesService.deleteService(id);
    }
}
