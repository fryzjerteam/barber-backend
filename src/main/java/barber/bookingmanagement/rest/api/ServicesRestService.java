package barber.bookingmanagement.rest.api;

import barber.bookingmanagement.dto.ServiceDto;
import barber.general.common.api.RestService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/barber/services")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ServicesRestService extends RestService {

  @GET
  @Path("/all")
  List<ServiceDto> getAllServices();

  @POST
  @Path("/service")
  List<ServiceDto> addService(ServiceDto serviceDto);

  @PUT
  @Path("/service")
  ServiceDto updateService(ServiceDto serviceDto);

  @DELETE
  @Path("/service")
  void deleteService(@QueryParam("id") String id);

}
