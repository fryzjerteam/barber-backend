package barber.bookingmanagement.rest.api;

import barber.bookingmanagement.dto.ServiceAssignmentDto;
import barber.bookingmanagement.dto.ServiceDto;
import barber.general.common.api.RestService;
import barber.general.common.api.to.UserDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/barber/serviceAssignment")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ServiceAssignmentRestService extends RestService {

  @POST
  @Path("/findByUser")
  List<ServiceDto> getServicesByUser(UserDto userDto);

  @POST
  @Path("/assignment")
  ServiceDto addAssignment(ServiceAssignmentDto serviceAssignmentDto);

  /*@PUT
  @Path("/assignment")
  ServiceDto updateService(ServiceDto serviceDto);*/

  @POST
  @Path("/deleteAssignment")
  void deleteAssignment(ServiceAssignmentDto serviceAssignmentDto);

}
