package barber.bookingmanagement.rest.api;

import barber.general.common.api.RestService;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.dto.UserDataDts;
import barber.usermanagement.dto.UserRegisterDataDts;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/applicationSettings")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ApplicationSettingsRestService extends RestService {

  @GET
  @Path("/{setting}")
  String getSetting(@PathParam("setting") String setting);

  @PUT
  @Path("/{setting}")
  String updateSetting(@PathParam("setting") String setting, @QueryParam("value") String value);

}
