package barber.bookingmanagement.rest.api;

import barber.bookingmanagement.dto.ScheduleDto;
import barber.general.common.api.RestService;
import barber.general.common.api.to.UserDto;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/barber/schedules")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface ScheduleRestService extends RestService {

  @POST
  @Path("/findByUser")
  List<ScheduleDto> findByUser(UserDto userDto);

  @POST
  @Path("/schedule")
  ScheduleDto addSchedule(ScheduleDto scheduleDto);

  @PUT
  @Path("/schedule")
  ScheduleDto updateSchedule(ScheduleDto scheduleDto);

  @DELETE
  @Path("/schedule")
  void deleteSchedule(@QueryParam("id") String id);

}
