package barber.bookingmanagement.rest.api;

import barber.bookingmanagement.dto.*;
import barber.general.common.api.RestService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/bookings")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface BookingRestService extends RestService {

  @POST
  @Path("/freeBookings")
  List<FreeBookingDto> getFreeBookings(SearchFreeBookingDts searchFreeBookingDts);

  @POST
  @Path("/booking")
  BookingDto createClientBooking(BookingDto bookingDto);

  @POST
  @Path("/bookingsByEmployee")
  List<BookingDto> getBookingsByEmployee(SearchBookingsDts searchBookingsDts);

  @POST
  @Path("/bookingsByDay")
  List<BookingDto> getBookingsByDay(SearchBookingsDts searchBookingsDts);

  @POST
  @Path("/bookingsCanceledByDay")
  List<BookingDto> getBookingsCanceledByDay(SearchBookingsDts searchBookingsDts);

  @POST
  @Path("/bookingsByService")
  List<BookingDto> getBookingsByService(SearchBookingsDts searchBookingsDts);

  @GET
  @Path("/futureClientBookings")
  List<BookingDto> getFutureClientBookings();

  @GET
  @Path("/oldClientBookings")
  List<BookingDto> getOldClientBookings();

  @POST
  @Path("/cancelBooking")
  void cancelBooking(CancelBookingDto cancelBookingDto);

  @POST
  @Path("/bookingsByCurrentEmployee")
  List<BookingDto> getBookingsByCurrentEmployee(SearchBookingsDts searchBookingsDts);

  /*//*@PUT
  @Path("/assignment")
  ServiceDto updateService(ServiceDto serviceDto);*//*

  */

}
