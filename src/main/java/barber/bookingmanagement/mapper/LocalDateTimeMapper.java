package barber.bookingmanagement.mapper;

import barber.bookingmanagement.dto.localdatetime.LocalDateDto;
import barber.bookingmanagement.dto.localdatetime.LocalDateTimeDto;
import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;
import org.mapstruct.Mapper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Mapper(componentModel = "spring")
public abstract class LocalDateTimeMapper {

    public LocalDateTime dateTimeDtoToDateTime(LocalDateTimeDto localDateTimeDto){
        if(localDateTimeDto == null) {
            return null;
        }
        return LocalDateTime.of(dateDtoToDate(localDateTimeDto.getLocalDateDto()), timeDtoToTime(localDateTimeDto.getLocalTimeDto()));
    }

    public LocalDateTimeDto dateTimeToDateTimeDto(LocalDateTime localDateTime){
        if(localDateTime == null){
            return null;
        }
        LocalDateTimeDto localDateTimeDto = new LocalDateTimeDto();
        localDateTimeDto.setLocalDateDto(dateToDateDto(localDateTime.toLocalDate()));
        localDateTimeDto.setLocalTimeDto(timeToTimeDto(localDateTime.toLocalTime()));
        return localDateTimeDto;
    }

    public LocalDate dateDtoToDate(LocalDateDto localDateDto){
        if(localDateDto == null){
            return null;
        }
        return LocalDate.of(localDateDto.getYear(), localDateDto.getMonth(), localDateDto.getDay());
    }

    public LocalDateDto dateToDateDto(LocalDate localDate){
        if(localDate == null){
            return null;
        }
        LocalDateDto localDateDto = new LocalDateDto();
        localDateDto.setYear(localDate.getYear());
        localDateDto.setMonth(localDate.getMonthValue());
        localDateDto.setDay(localDate.getDayOfMonth());
        return localDateDto;
    }

    public LocalTime timeDtoToTime(LocalTimeDto localTimeDto){
        if(localTimeDto == null){
            return null;
        }
        return LocalTime.of(localTimeDto.getHour(), localTimeDto.getMinute(), localTimeDto.getSecond());
    }

    public LocalTimeDto timeToTimeDto(LocalTime localTime){
        if(localTime == null){
            return null;
        }
        LocalTimeDto localTimeDto = new LocalTimeDto();
        localTimeDto.setHour(localTime.getHour());
        localTimeDto.setMinute(localTime.getMinute());
        localTimeDto.setSecond(localTime.getSecond());
        return localTimeDto;
    }

}
