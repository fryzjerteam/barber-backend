package barber.bookingmanagement.mapper;

import barber.bookingmanagement.dto.BookingDto;
import barber.bookingmanagement.dto.ServiceAssignmentDto;
import barber.bookingmanagement.dto.localdatetime.LocalDateTimeDto;
import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;
import barber.bookingmanagement.entity.Booking;
import barber.bookingmanagement.entity.ServiceAssignment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class BookingMapper {

    @Autowired
    LocalDateTimeMapper localDateTimeMapper;
    @Autowired
    ServiceAssignmentMapper serviceAssignmentMapper;

    @Mapping(target = "client", ignore = true)
    @Mapping(target = "modificationCounter", ignore = true)
    @Mapping(target = "revision", ignore = true)
    @Mapping(target = "serviceAssignment", source = "serviceAssignmentDto")
    @Mapping(target = "startTime", source = "startDateTimeDto")
    public abstract Booking bookingDtoToBooking(BookingDto bookingDto);

    @Mapping(target = "serviceAssignmentDto", source = "serviceAssignment")
    @Mapping(target = "startDateTimeDto", source = "startTime")
    public abstract BookingDto bookingToBookingDto(Booking booking);

    protected ServiceAssignment serviceAssignmentDtoToServiceAssignment(ServiceAssignmentDto serviceAssignmentDto){
        return serviceAssignmentMapper.assignmentDtoToAssignment(serviceAssignmentDto);
    }

    protected ServiceAssignmentDto serviceAssignmentToServiceAssignmentDto(ServiceAssignment serviceAssignment){
        return serviceAssignmentMapper.assignmentToAssignmentDto(serviceAssignment);
    }

    protected LocalDateTime localDateTimeDtoToLocalDateTime(LocalDateTimeDto localDateTimeDto){
        return localDateTimeMapper.dateTimeDtoToDateTime(localDateTimeDto);
    }

    protected LocalDateTimeDto localDateTimeToLocalDateTimeDto(LocalDateTime localDateTime){
        return localDateTimeMapper.dateTimeToDateTimeDto(localDateTime);
    }

    public abstract List<BookingDto> bookingsToBookingDtos(List<Booking> bookings);
}
