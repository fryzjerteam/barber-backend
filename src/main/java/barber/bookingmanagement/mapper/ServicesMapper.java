package barber.bookingmanagement.mapper;

import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.entity.Service;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ServicesMapper {
    List<ServiceDto> servicesToServicesDto(List<Service> services);

    @Mapping(ignore = true, target = "revision")
    @Mapping(ignore = true, target = "modificationCounter")
    @Mapping(ignore = true, target = "deactivate")
    Service serviceDtoToService(ServiceDto serviceDto);
    ServiceDto serviceToServiceDto(Service service);

    @Mapping(target = "modificationCounter", ignore = true)
    @Mapping(target = "revision", ignore = true)
    @Mapping(ignore = true, target = "deactivate")
    void updateServiceFromDto(ServiceDto serviceDto, @MappingTarget Service service);
}
