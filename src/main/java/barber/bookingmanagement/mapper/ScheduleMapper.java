package barber.bookingmanagement.mapper;

import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;
import barber.bookingmanagement.dto.ScheduleDayDto;
import barber.bookingmanagement.dto.ScheduleDto;
import barber.bookingmanagement.entity.Schedule;
import barber.bookingmanagement.entity.ScheduleDay;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ScheduleMapper {


    public static final String MONDAY = "MONDAY";
    public static final String TUESDAY = "TUESDAY";
    public static final String WEDNESDAY = "WEDNESDAY";
    public static final String THURSDAY = "THURSDAY";
    public static final String FRIDAY = "FRIDAY";
    public static final String SATURDAY = "SATURDAY";
    public static final String SUNDAY = "SUNDAY";

    @Autowired
    LocalDateTimeMapper localDateTimeMapper;

    @Mapping(target = "employee", ignore = true)
    @Mapping(target = "modificationCounter", ignore = true)
    @Mapping(target = "revision", ignore = true)
    public Schedule scheduleDtoToSchedule(ScheduleDto scheduleDto) {
        if (scheduleDto == null) return null;

        Schedule schedule = new Schedule();
        schedule.setId(scheduleDto.getId());

        return updateScheduleDays(scheduleDto, schedule);
    }

    private Schedule updateScheduleDays(ScheduleDto sourceScheduleDto, Schedule targetSchedule) {
        List<ScheduleDayDto> days = sourceScheduleDto.getDays();
        if (days != null) {
            ScheduleDayDto scheduleDayDto = days.stream().filter(day -> MONDAY.equals(day.getDayName())).findFirst().orElse(null);
            targetSchedule.setMonday(scheduleDayDtoToScheduleDay(scheduleDayDto));
            scheduleDayDto = days.stream().filter(day -> TUESDAY.equals(day.getDayName())).findFirst().orElse(null);
            targetSchedule.setTuesday(scheduleDayDtoToScheduleDay(scheduleDayDto));
            scheduleDayDto = days.stream().filter(day -> WEDNESDAY.equals(day.getDayName())).findFirst().orElse(null);
            targetSchedule.setWednesday(scheduleDayDtoToScheduleDay(scheduleDayDto));
            scheduleDayDto = days.stream().filter(day -> THURSDAY.equals(day.getDayName())).findFirst().orElse(null);
            targetSchedule.setThursday(scheduleDayDtoToScheduleDay(scheduleDayDto));
            scheduleDayDto = days.stream().filter(day -> FRIDAY.equals(day.getDayName())).findFirst().orElse(null);
            targetSchedule.setFriday(scheduleDayDtoToScheduleDay(scheduleDayDto));
            scheduleDayDto = days.stream().filter(day -> SATURDAY.equals(day.getDayName())).findFirst().orElse(null);
            targetSchedule.setSaturday(scheduleDayDtoToScheduleDay(scheduleDayDto));
            scheduleDayDto = days.stream().filter(day -> SUNDAY.equals(day.getDayName())).findFirst().orElse(null);
            targetSchedule.setSunday(scheduleDayDtoToScheduleDay(scheduleDayDto));
        }
        return targetSchedule;
    }

    @Mapping(target = "userDto", ignore = true)
    public  ScheduleDto scheduleToScheduleDto(Schedule schedule){
        if(schedule == null ) return null;

        ScheduleDto scheduleDto = new ScheduleDto();
        scheduleDto.setId(schedule.getId());

        List<ScheduleDayDto> days = new ArrayList<>();

        ScheduleDayDto monday = scheduleDayToScheduleDayDto(schedule.getMonday());
        monday.setDayName(MONDAY);
        days.add(monday);
        ScheduleDayDto tuesday = scheduleDayToScheduleDayDto(schedule.getTuesday());
        tuesday.setDayName(TUESDAY);
        days.add(tuesday);
        ScheduleDayDto wednesday = scheduleDayToScheduleDayDto(schedule.getWednesday());
        wednesday.setDayName(WEDNESDAY);
        days.add(wednesday);
        ScheduleDayDto thurday = scheduleDayToScheduleDayDto(schedule.getThursday());
        thurday.setDayName(THURSDAY);
        days.add(thurday);
        ScheduleDayDto friday = scheduleDayToScheduleDayDto(schedule.getFriday());
        friday.setDayName(FRIDAY);
        days.add(friday);
        ScheduleDayDto saturday = scheduleDayToScheduleDayDto(schedule.getSaturday());
        saturday.setDayName(SATURDAY);
        days.add(saturday);
        ScheduleDayDto sunday = scheduleDayToScheduleDayDto(schedule.getSunday());
        sunday.setDayName(SUNDAY);
        days.add(sunday);
        scheduleDto.setDays(days);

        return scheduleDto;
    }

    @Mapping(target = "employee", ignore = true)
    @Mapping(target = "modificationCounter", ignore = true)
    @Mapping(target = "revision", ignore = true)
    public void updateScheduleFromDto(ScheduleDto scheduleDto, @MappingTarget Schedule schedule){
        if ( scheduleDto == null ) {
            return;
        }
        schedule.setId( scheduleDto.getId() );
        updateScheduleDays(scheduleDto, schedule);
    }

    public abstract List<ScheduleDto> schedulesToSchedulesDto(List<Schedule> schedules);

    public abstract ScheduleDay scheduleDayDtoToScheduleDay(ScheduleDayDto scheduleDayDto);

    @Mapping(target = "dayName", ignore = true)
    public abstract ScheduleDayDto scheduleDayToScheduleDayDto(ScheduleDay scheduleDay);

    protected LocalTime localTimeDtoToLocalTime(LocalTimeDto localTimeDto){
        return localDateTimeMapper.timeDtoToTime(localTimeDto);
    }

    protected LocalTimeDto localTimeToLocalTimeDto(LocalTime localTime){
        return localDateTimeMapper.timeToTimeDto(localTime);
    }
}
