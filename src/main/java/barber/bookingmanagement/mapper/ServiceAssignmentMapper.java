package barber.bookingmanagement.mapper;

import barber.bookingmanagement.dto.ScheduleDayDto;
import barber.bookingmanagement.dto.ScheduleDto;
import barber.bookingmanagement.dto.ServiceAssignmentDto;
import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;
import barber.bookingmanagement.entity.Schedule;
import barber.bookingmanagement.entity.ScheduleDay;
import barber.bookingmanagement.entity.Service;
import barber.bookingmanagement.entity.ServiceAssignment;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.entity.BasicUser;
import barber.usermanagement.mapper.UserMapper;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public abstract class ServiceAssignmentMapper {

    @Autowired
    UserMapper userMapper;
    @Autowired
    ServicesMapper servicesMapper;

    @Mapping(target = "serviceDto", source = "service")
    @Mapping(target = "userDto", source = "employee")
    public abstract ServiceAssignmentDto assignmentToAssignmentDto(ServiceAssignment serviceAssignment);

    protected UserDto basicUserToUserDto(BasicUser basicUser){
        return userMapper.basicUserToUserDto(basicUser);
    }
    protected ServiceDto serviceToServiceDto(Service service){
        return servicesMapper.serviceToServiceDto(service);
    }

    @Mapping(target = "modificationCounter", ignore = true)
    @Mapping(target = "revision", ignore = true)
    @Mapping(target = "service", source = "serviceDto")
    @Mapping(target = "employee", source = "userDto")
    @Mapping(target = "deactivate", ignore = true)
    public abstract ServiceAssignment assignmentDtoToAssignment(ServiceAssignmentDto serviceAssignmentDto);

    protected BasicUser userDtoToBasicUser(UserDto userDto){
        return userMapper.userDtotoBasicUser(userDto);
    }
    protected Service serviceDtoToService(ServiceDto serviceDto){
        return servicesMapper.serviceDtoToService(serviceDto);
    }
}
