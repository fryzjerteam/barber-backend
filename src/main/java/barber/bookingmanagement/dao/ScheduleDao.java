package barber.bookingmanagement.dao;

import barber.bookingmanagement.dto.ScheduleDto;
import barber.bookingmanagement.entity.Schedule;
import barber.general.dataaccess.api.dao.ApplicationDao;
import barber.usermanagement.entity.BasicUser;

import java.util.List;

public interface ScheduleDao extends ApplicationDao<Schedule> {
    List<Schedule> findScheduleByEmployee(BasicUser basicUser);
}
