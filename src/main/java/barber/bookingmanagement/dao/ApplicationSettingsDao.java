package barber.bookingmanagement.dao;

import barber.bookingmanagement.entity.ApplicationSettings;
import barber.general.dataaccess.api.dao.ApplicationDao;

public interface ApplicationSettingsDao extends ApplicationDao<ApplicationSettings> {
}
