package barber.bookingmanagement.dao;

import barber.bookingmanagement.entity.Service;
import barber.bookingmanagement.entity.ServiceAssignment;
import barber.general.dataaccess.api.dao.ApplicationDao;
import barber.usermanagement.entity.BasicUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ServiceAssignmentDao extends ApplicationDao<ServiceAssignment> {
    @Query("select s from ServiceAssignment s where s.employee=:employee and s.deactivate=false")
    List<ServiceAssignment> findByEmployee(@Param("employee") BasicUser employee);

    @Query("select s from ServiceAssignment s where s.service=:service and s.deactivate=false")
    List<ServiceAssignment> findByService(@Param("service") Service service);

    @Query("select s from ServiceAssignment s where s.employee=:employee and s.service=:service and s.deactivate=false")
    ServiceAssignment findByEmployeeAndService(@Param("employee") BasicUser employee, @Param("service") Service service);

    @Query("select s from ServiceAssignment s where s.id=:id and s.deactivate=false")
    ServiceAssignment findOne(@Param("id") Long id);
}
