package barber.bookingmanagement.dao;

import barber.bookingmanagement.entity.Booking;
import barber.bookingmanagement.common.BookingState;
import barber.bookingmanagement.entity.Service;
import barber.general.dataaccess.api.dao.ApplicationDao;
import barber.usermanagement.entity.BasicUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.LocalDateTime;
import java.util.List;

public interface BookingDao extends ApplicationDao<Booking> {
    List<Booking> findBookingByStartTimeBetween(LocalDateTime start, LocalDateTime end);

    @Query("select b from Booking b " +
            "where b.serviceAssignment.service = :service " +
            "and b.startTime > :start " +
            "and b.startTime < :end " +
            "and b.bookingState=barber.bookingmanagement.common.BookingState.ACTIVE")
    List<Booking> findBookingByServiceAndTime(@Param("service") Service service, @Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

    @Query("select b from Booking b " +
            "where b.serviceAssignment.employee = :employee " +
            "and b.startTime > :start " +
            "and b.startTime < :end " +
            "and b.bookingState=barber.bookingmanagement.common.BookingState.ACTIVE")
    List<Booking> findBookingByEmployeeAndTime(@Param("employee") BasicUser employee, @Param("start") LocalDateTime start, @Param("end") LocalDateTime end);

    List<Booking> findBookingByClientAndStartTimeAfter(BasicUser client, LocalDateTime startTime);

    List<Booking> findBookingByStartTimeBetweenAndBookingStateIs(LocalDateTime start, LocalDateTime end, BookingState bookingState);

    List<Booking> findBookingByClientAndStartTimeBefore(BasicUser client, LocalDateTime startTime);
}
