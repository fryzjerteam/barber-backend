package barber.bookingmanagement.dao;

import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.entity.Service;
import barber.general.dataaccess.api.dao.ApplicationDao;
import barber.usermanagement.entity.BasicUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ServicesDao extends ApplicationDao<Service> {
    @Query("select s from Service s where s.deactivate=false")
    List<Service> findAll();

    @Query("select s from Service s where s.id=:id and s.deactivate=false")
    Service findOne(@Param("id") Long id);
}
