package barber.bookingmanagement.service;

import barber.bookingmanagement.dto.ScheduleDto;
import barber.bookingmanagement.entity.Schedule;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.entity.BasicUser;

import java.util.List;

public interface ScheduleService {

    void createEmptySchedule(BasicUser basicUser);

    List<ScheduleDto> findScheduleByUser(UserDto userDto);
    List<Schedule> findScheduleByUser(BasicUser basicUser);

    ScheduleDto addSchedule(ScheduleDto scheduleDto);

    ScheduleDto updateSchedule(ScheduleDto scheduleDto);

    void deleteSchedule(String id);
}
