package barber.bookingmanagement.service.impl;

import barber.bookingmanagement.common.AppSettingsPath;
import barber.bookingmanagement.common.BookingState;
import barber.bookingmanagement.dao.BookingDao;
import barber.bookingmanagement.dto.*;
import barber.bookingmanagement.dto.localdatetime.LocalDateDto;
import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;
import barber.bookingmanagement.entity.*;
import barber.bookingmanagement.mapper.LocalDateTimeMapper;
import barber.bookingmanagement.mapper.ScheduleMapper;
import barber.bookingmanagement.mapper.ServiceAssignmentMapper;
import barber.bookingmanagement.mapper.ServicesMapper;
import barber.bookingmanagement.service.ApplicationSettingsService;
import barber.bookingmanagement.service.ScheduleService;
import barber.bookingmanagement.service.ServiceAssignmentService;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import com.google.common.collect.Lists;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Component
public class SearchFreeBookingSupport {

    @Autowired
    private BookingDao bookingDao;
    @Autowired
    private ServicesMapper servicesMapper;
    @Autowired
    private LocalDateTimeMapper dateTimeMapper;
    @Autowired
    private ServiceAssignmentService serviceAssignmentService;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private ApplicationSettingsService appSettingsService;
    @Autowired
    private ScheduleMapper scheduleMapper;
    @Autowired
    private ServiceAssignmentMapper serviceAssignMapper;

    public List<FreeBookingDto> searchFreeBookings(SearchFreeBookingDts searchFreeBookingDts){

        if(searchFreeBookingDts.getServiceDto() != null && searchFreeBookingDts.getLocalDateDto() != null){
            List<ServiceAssignment> serviceAssignments = findServiceAssignmentsForService(searchFreeBookingDts.getServiceDto());
            List<Booking> allBookingsForDay = findAllActiveBookingsByDate(searchFreeBookingDts.getLocalDateDto());
            List<FreeBookingDto> allFreeBookings = new ArrayList<>();

            for(ServiceAssignment serviceAssignment : serviceAssignments){
                List<Booking> employeeBookings = allBookingsForDay.stream()
                        .filter(b -> b.getServiceAssignment().getEmployee().equals(serviceAssignment.getEmployee()))
                        .sorted(Comparator.comparing(Booking::getStartTime))
                        .collect(Collectors.toList());
                FreeBookingDto freeBookingsForEmployee = getFreeBookingsForEmployee(serviceAssignment, employeeBookings, searchFreeBookingDts);
                if(freeBookingsForEmployee != null){
                    allFreeBookings.add(freeBookingsForEmployee);
                }
            }
            return allFreeBookings;
        }
        throw new WrongDataReceivedException();
    }

    private FreeBookingDto getFreeBookingsForEmployee(ServiceAssignment serviceAssignment, List<Booking> employeeBookings, SearchFreeBookingDts searchFreeBookingDts) {

        List<Schedule> scheduleByUser = scheduleService.findScheduleByUser(serviceAssignment.getEmployee());
        Schedule employeeSchedule = scheduleByUser.stream().findFirst().orElse(null);
        if(employeeSchedule != null){
            LocalDate serviceDay = dateTimeMapper.dateDtoToDate(searchFreeBookingDts.getLocalDateDto());
            String dayName = serviceDay.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.ENGLISH);
            ScheduleDto employeeScheduleDto = scheduleMapper.scheduleToScheduleDto(employeeSchedule);
            ScheduleDayDto scheduleDayDto = employeeScheduleDto.getDays().stream()
                    .filter(day -> StringUtils.equalsIgnoreCase(day.getDayName(), dayName))
                    .findFirst().orElse(null);

            Duration serviceTime = getServiceTime(searchFreeBookingDts.getServiceDto());
            ServiceAssignmentDto serviceAssignmentDto = serviceAssignMapper.assignmentToAssignmentDto(serviceAssignment);

            FreeBookingDto freeBookingDto;
            if(employeeBookings.isEmpty()){
                freeBookingDto = buildEmptyDayFreeBookings(serviceAssignmentDto, scheduleDayDto, serviceTime);
            }
            else {
                freeBookingDto = buildFreeBookings(serviceAssignmentDto, scheduleDayDto, serviceTime, employeeBookings);
            }
            return filterPastTimes(freeBookingDto, searchFreeBookingDts.getLocalDateDto());
        }
        return null;
    }

    private FreeBookingDto filterPastTimes(FreeBookingDto freeBookingDto, LocalDateDto localDateDto) {
        LocalDate localDate = dateTimeMapper.dateDtoToDate(localDateDto);
        if(localDate.isEqual(LocalDate.now())){
            List<LocalTimeDto> startTimes = freeBookingDto.getStartTimes().stream()
                    .filter(time -> {
                        LocalTime localTime = dateTimeMapper.timeDtoToTime(time);
                        return localTime.isAfter(LocalTime.now());
                    })
                    .collect(Collectors.toList());

            if(startTimes.isEmpty()) return null;
            FreeBookingDto filteredBooking = new FreeBookingDto();
            filteredBooking.setServiceAssignmentDto(freeBookingDto.getServiceAssignmentDto());
            filteredBooking.setStartTimes(startTimes);
            return filteredBooking;
        }
        return freeBookingDto;
    }

    private FreeBookingDto buildFreeBookings(ServiceAssignmentDto serviceAssignmentDto, ScheduleDayDto scheduleDayDto, Duration serviceTime, List<Booking> employeeBookings) {
        LocalTime startSchedule = dateTimeMapper.timeDtoToTime(scheduleDayDto.getStartTime());
        LocalTime endSchedule = dateTimeMapper.timeDtoToTime(scheduleDayDto.getEndTime());

        LocalTime startTime = startSchedule;
        LocalTime endTime = employeeBookings.get(0).getStartTime().toLocalTime();
        List<LocalTimeDto> startTimes = getFreeBookingBetweenTime(serviceTime, startTime, endTime);

        for(int i = 0; i < employeeBookings.size(); i++){
            Service currentService = employeeBookings.get(i).getServiceAssignment().getService();
            Duration currentServiceTime = getServiceTime(currentService);
            startTime = employeeBookings.get(i).getStartTime().toLocalTime().plus(currentServiceTime);

            if( i != employeeBookings.size()-1){
                endTime = employeeBookings.get(i+1).getStartTime().toLocalTime();
            }
            else {
                endTime = endSchedule;
            }
            startTimes.addAll(getFreeBookingBetweenTime(serviceTime,startTime, endTime));
        }

        FreeBookingDto freeBookings = new FreeBookingDto();
        freeBookings.setStartTimes(startTimes);
        freeBookings.setServiceAssignmentDto(serviceAssignmentDto);
        return freeBookings;
    }

    private FreeBookingDto buildEmptyDayFreeBookings(ServiceAssignmentDto serviceAssignmentDto, ScheduleDayDto scheduleDayDto, Duration serviceTime) {
        LocalTime startTime = dateTimeMapper.timeDtoToTime(scheduleDayDto.getStartTime());
        LocalTime endTime = dateTimeMapper.timeDtoToTime(scheduleDayDto.getEndTime());

        FreeBookingDto freeBookingDto = new FreeBookingDto();
        freeBookingDto.setServiceAssignmentDto(serviceAssignmentDto);
        freeBookingDto.setStartTimes(getFreeBookingBetweenTime(serviceTime, startTime, endTime));
        return freeBookingDto;
    }

    private List<LocalTimeDto> getFreeBookingBetweenTime(Duration serviceTime, LocalTime startTime, LocalTime endTime) {
        List<LocalTimeDto> startTimes = Lists.newArrayList();
        LocalTime time = startTime;
        while (time.plus(serviceTime).compareTo(endTime) <= 0){
            startTimes.add(dateTimeMapper.timeToTimeDto(time));
            time = time.plus(serviceTime);
        }
        return startTimes;
    }

    public Duration getServiceTime(Service service){
        if(service == null){
            return null;
        }
        return getServiceTime(service.getDurationUnits());
    }
    public Duration getServiceTime(ServiceDto serviceDto){
        if(serviceDto == null){
            return null;
        }
        return getServiceTime(serviceDto.getDurationUnits());
    }

    private Duration getServiceTime(Integer durationUnits) {
        int appMinutes = Integer.parseInt(appSettingsService.getSetting(AppSettingsPath.UNIT_OFFSET.getPath()));
        int serviceMinutes = appMinutes * durationUnits;
        return Duration.ofMinutes(serviceMinutes);
    }

    private List<ServiceAssignment> findServiceAssignmentsForService(ServiceDto serviceDto) {
        Service service = servicesMapper.serviceDtoToService(serviceDto);
        return serviceAssignmentService.findServiceAssignmentsByService(service);
    }

    public List<Booking> findAllActiveBookingsByDate(LocalDateDto localDateDto) {
        LocalDate localDate = dateTimeMapper.dateDtoToDate(localDateDto);
        return bookingDao.findBookingByStartTimeBetweenAndBookingStateIs(LocalDateTime.of(localDate, LocalTime.of(0, 0)), LocalDateTime.of(localDate, LocalTime.of(23, 59)), BookingState.ACTIVE);
    }
}
