package barber.bookingmanagement.service.impl;

import barber.bookingmanagement.common.AppSettingsPath;
import barber.bookingmanagement.dao.ApplicationSettingsDao;
import barber.bookingmanagement.entity.ApplicationSettings;
import barber.bookingmanagement.service.ApplicationSettingsService;
import barber.errorhandling.httpstatus.CorruptDataException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.OffsetTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Service
public class ApplicationSettingsServiceImpl implements ApplicationSettingsService {


    @Autowired
    ApplicationSettingsDao applicationSettingsDao;

    @Override
    public String getSetting(String setting) {

        if(AppSettingsPath.contains(setting)){
            List<ApplicationSettings> appSettings  = applicationSettingsDao.findAll();
            // should be only one row
            if(!appSettings.isEmpty()){
                ApplicationSettings appSetting = appSettings.get(0);

                if(AppSettingsPath.UNIT_OFFSET.getPath().equals(setting)){
                    OffsetTime unitOffset = appSetting.getUnitOffset();
                    OffsetTime truncated = unitOffset.truncatedTo(ChronoUnit.MINUTES);
                    int minute = truncated.getMinute();
                    return Integer.toString(minute);
                }

                // here next setting path

            }
            throw new CorruptDataException();
        }
        throw new WrongDataReceivedException();
    }
}
