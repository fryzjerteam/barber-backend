package barber.bookingmanagement.service.impl;

import barber.bookingmanagement.dao.ServiceAssignmentDao;
import barber.bookingmanagement.dto.ServiceAssignmentDto;
import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.entity.Service;
import barber.bookingmanagement.entity.ServiceAssignment;
import barber.bookingmanagement.mapper.ServicesMapper;
import barber.bookingmanagement.service.ServiceAssignmentService;
import barber.errorhandling.httpstatus.CorruptDataException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.entity.BasicUser;
import barber.usermanagement.mapper.UserMapper;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class ServiceAssignmentServiceImpl implements ServiceAssignmentService {

    @Autowired
    ServicesMapper servicesMapper;

    @Autowired
    UserMapper userMapper;

    @Autowired
    ServiceAssignmentDao serviceAssignmentDao;

    @Override
    public ServiceDto addAssignment(ServiceAssignmentDto serviceAssignmentDto) {
        Preconditions.checkNotNull(serviceAssignmentDto);

        ServiceDto serviceDto = serviceAssignmentDto.getServiceDto();
        UserDto userDto = serviceAssignmentDto.getUserDto();

        if(serviceDto != null && userDto != null && serviceDto.getId() != null && userDto.getId() != null) {
            ServiceAssignment newAssignment = new ServiceAssignment();
            Service service = servicesMapper.serviceDtoToService(serviceDto);
            BasicUser employee = userMapper.userDtotoBasicUser(userDto);
            ServiceAssignment assignment = serviceAssignmentDao.findByEmployeeAndService(employee, service);
            if(assignment == null){
                newAssignment.setService(service);
                newAssignment.setEmployee(employee);
                serviceAssignmentDao.save(newAssignment);
                return serviceDto;
            }
            throw new WrongDataReceivedException(); // TODO: 28.10.18 Zwróć nulla ? i Potem obsluz na kliencie?
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public List<ServiceAssignment> findServiceAssignmentsByService(Service service) {
        Preconditions.checkNotNull(service);
        return serviceAssignmentDao.findByService(service);
    }

    @Override
    public void deactivateAssignments(Service service) {
        Preconditions.checkNotNull(service);

        List<ServiceAssignment> serviceAssignments = serviceAssignmentDao.findByService(service);
        for(ServiceAssignment serviceAssignment : serviceAssignments){
            serviceAssignment.setDeactivate(true);
            serviceAssignmentDao.save(serviceAssignment);
        }
    }

    @Override
    public void deleteAssignment(ServiceAssignmentDto serviceAssignmentDto) {
        Preconditions.checkNotNull(serviceAssignmentDto);

        ServiceDto serviceDto = serviceAssignmentDto.getServiceDto();
        UserDto userDto = serviceAssignmentDto.getUserDto();
        if(serviceDto != null && userDto != null){
            Service service = servicesMapper.serviceDtoToService(serviceDto);
            BasicUser basicUser = userMapper.userDtotoBasicUser(userDto);
            ServiceAssignment serviceAssignment = serviceAssignmentDao.findByEmployeeAndService(basicUser,service);
            if(serviceAssignment != null){
                serviceAssignment.setDeactivate(true);
                serviceAssignmentDao.save(serviceAssignment);
                return;
            }
            throw new CorruptDataException();
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public List<ServiceDto> getServicesByUser(UserDto userDto) {
        if(userDto != null && userDto.getId() != null){
            List<ServiceAssignment> serviceAssingnments= serviceAssignmentDao.findByEmployee(userMapper.userDtotoBasicUser(userDto));
            List<Service> services = serviceAssingnments.stream().map(ServiceAssignment::getService).collect(Collectors.toList());
            return servicesMapper.servicesToServicesDto(services);
        }
        throw new WrongDataReceivedException();
    }

}
