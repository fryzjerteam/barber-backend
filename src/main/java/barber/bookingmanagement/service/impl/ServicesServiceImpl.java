package barber.bookingmanagement.service.impl;

import barber.bookingmanagement.dao.ServicesDao;
import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.entity.ServiceAssignment;
import barber.bookingmanagement.mapper.ServicesMapper;
import barber.bookingmanagement.service.ServiceAssignmentService;
import barber.bookingmanagement.service.ServicesService;
import barber.errorhandling.httpstatus.DataNotFoundException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ServicesServiceImpl implements ServicesService {

    @Autowired
    ServicesDao servicesDao;

    @Autowired
    ServicesMapper servicesMapper;

    @Autowired
    ServiceAssignmentService serviceAssignmentService;

    @Override
    public List<ServiceDto> getAllServices() {
        List<barber.bookingmanagement.entity.Service> services = servicesDao.findAll();
        return servicesMapper.servicesToServicesDto(services);
    }

    @Override
    public List<ServiceDto> addService(ServiceDto serviceDto) {
        Preconditions.checkNotNull(serviceDto);
        servicesDao.save(servicesMapper.serviceDtoToService(serviceDto));
        return getAllServices();
    }

    @Override
    public ServiceDto updateService(ServiceDto serviceDto) {
        Preconditions.checkNotNull(serviceDto);
        if(serviceDto.getId() != null){
            barber.bookingmanagement.entity.Service foundService = servicesDao.findOne(serviceDto.getId());
            servicesMapper.updateServiceFromDto(serviceDto, foundService);
            barber.bookingmanagement.entity.Service saved = servicesDao.save(foundService);
            return servicesMapper.serviceToServiceDto(saved);
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public void deleteService(String id) {
        if(id != null && !id.isEmpty()){
            barber.bookingmanagement.entity.Service service = servicesDao.findOne(Long.parseLong(id));
            if(service != null){
                serviceAssignmentService.deactivateAssignments(service);
                service.setDeactivate(true);
                servicesDao.save(service);
                return;
            }
            throw new DataNotFoundException();
        }
        throw new WrongDataReceivedException();
    }
}
