package barber.bookingmanagement.service.impl;

import barber.bookingmanagement.dao.BookingDao;
import barber.bookingmanagement.dto.*;
import barber.bookingmanagement.dto.localdatetime.LocalTimeDto;
import barber.bookingmanagement.entity.Booking;
import barber.bookingmanagement.common.BookingState;
import barber.bookingmanagement.entity.Service;
import barber.bookingmanagement.entity.ServiceAssignment;
import barber.bookingmanagement.mapper.BookingMapper;
import barber.bookingmanagement.mapper.LocalDateTimeMapper;
import barber.bookingmanagement.mapper.ServiceAssignmentMapper;
import barber.bookingmanagement.mapper.ServicesMapper;
import barber.bookingmanagement.service.BookingService;
import barber.errorhandling.httpstatus.CorruptDataException;
import barber.errorhandling.httpstatus.DataNotFoundException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.general.common.api.datatype.Role;
import barber.general.common.api.security.UserData;
import barber.usermanagement.entity.BasicUser;
import barber.usermanagement.mapper.UserMapper;
import barber.usermanagement.service.UserService;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.tuple.MutablePair;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Service
public class BookingServiceImpl implements BookingService {

    @Autowired
    private BookingDao bookingDao;
    @Autowired
    private SearchFreeBookingSupport searchFreeBookingSupport;
    @Autowired
    private ServiceAssignmentMapper serviceAssignmentMapper;
    @Autowired
    private LocalDateTimeMapper dateTimeMapper;
    @Autowired
    private BookingMapper bookingMapper;
    @Autowired
    private UserService userService;
    @Autowired
    private ServicesMapper servicesMapper;
    @Autowired
    private UserMapper userMapper;


    @Override
    public List<FreeBookingDto> findFreeBookings(SearchFreeBookingDts searchFreeBookingDts) {
        Preconditions.checkNotNull(searchFreeBookingDts);
        return searchFreeBookingSupport.searchFreeBookings(searchFreeBookingDts);
    }

    @Override
    public BookingDto createClientBooking(BookingDto bookingDto) {
        Preconditions.checkNotNull(bookingDto);

        List<Booking> allBookingsByDate = searchFreeBookingSupport.findAllActiveBookingsByDate(bookingDto.getStartDateTimeDto().getLocalDateDto());
        ServiceAssignment serviceAssignment = serviceAssignmentMapper.assignmentDtoToAssignment(bookingDto.getServiceAssignmentDto());
        List<Booking> employeeBookings = allBookingsByDate.stream()
                .filter(b -> b.getServiceAssignment().getEmployee().equals(serviceAssignment.getEmployee()))
                .collect(Collectors.toList());


        LocalTimeDto localTimeDto = bookingDto.getStartDateTimeDto().getLocalTimeDto();
        LocalTime time = dateTimeMapper.timeDtoToTime(localTimeDto);
        Duration serviceDuration = searchFreeBookingSupport.getServiceTime(serviceAssignment.getService());
        boolean isOverlaping = false;

        for (Booking booking : employeeBookings) {
            Service currentService = booking.getServiceAssignment().getService();
            Duration currentServiceDuration = searchFreeBookingSupport.getServiceTime(currentService);
            LocalTime currentServiceTime = booking.getStartTime().toLocalTime();

            isOverlaping |= checkTimeOverlaping(time, time.plus(serviceDuration), currentServiceTime, currentServiceTime.plus(currentServiceDuration));
        }

        if (!isOverlaping) {
            Booking booking = bookingMapper.bookingDtoToBooking(bookingDto);
            BasicUser user = userService.findCurrentUser();
            if (user != null) {
                booking.setClient(user);
                booking.setBookingState(BookingState.ACTIVE);
                Booking savedBooking = bookingDao.save(booking);
                return bookingMapper.bookingToBookingDto(savedBooking);
            }
            throw new CorruptDataException();
        }
        return null;
    }



    @Override
    public List<BookingDto> findBookingsByEmployee(SearchBookingsDts searchBookingsDts) {
        Preconditions.checkNotNull(searchBookingsDts);

        if (searchBookingsDts.getUserDto() != null) {
            BasicUser basicUser = userMapper.userDtotoBasicUser(searchBookingsDts.getUserDto());
            return findBookingsByBasicUser(searchBookingsDts, basicUser);
        }
        throw new WrongDataReceivedException();
    }

    private List<BookingDto> findBookingsByBasicUser(SearchBookingsDts searchBookingsDts, BasicUser basicUser) {
        MutablePair<LocalDateTime, LocalDateTime> timePair = mapSearchDatesToDateTimes(searchBookingsDts);
        List<Booking> bookings = bookingDao.findBookingByEmployeeAndTime(
                basicUser, timePair.getLeft(), timePair.getRight());
        List<Booking> sortedBookings = sortBookingsByTimeAsc(bookings);
        return bookingMapper.bookingsToBookingDtos(sortedBookings);
    }

    @Override
    public List<BookingDto> findBookingsByDay(SearchBookingsDts searchBookingsDts) {
        Preconditions.checkNotNull(searchBookingsDts);
        return findBookingsByDayWithStatus(searchBookingsDts, BookingState.ACTIVE);
    }

    private List<BookingDto> findBookingsByDayWithStatus(SearchBookingsDts searchBookingsDts, BookingState bookingState) {
        MutablePair<LocalDateTime, LocalDateTime> dateTimePair = mapSearchDatesToDateTimes(searchBookingsDts);
        List<Booking> unsortedBookings = bookingDao.findBookingByStartTimeBetweenAndBookingStateIs(
                dateTimePair.getLeft(), dateTimePair.getRight(), bookingState);
        List<Booking> sortedBookings = sortBookingsByTimeAsc(unsortedBookings);
        return bookingMapper.bookingsToBookingDtos(sortedBookings);
    }

    private List<Booking> sortBookingsByTimeAsc(List<Booking> unsortedBookings) {
        return unsortedBookings.stream()
                .sorted(Comparator.comparing(Booking::getStartTime))
                .collect(Collectors.toList());
    }
    private List<Booking> sortBookingsByTimeDesc(List<Booking> unsortedBookings) {
        return unsortedBookings.stream()
                .sorted(Comparator.comparing(Booking::getStartTime, Comparator.reverseOrder()))
                .collect(Collectors.toList());
    }

    private MutablePair<LocalDateTime, LocalDateTime> mapSearchDatesToDateTimes(SearchBookingsDts searchBookingsDts) {
        if (searchBookingsDts.getStartDate() != null && searchBookingsDts.getEndDate() != null) {
            LocalDate startDate = dateTimeMapper.dateDtoToDate(searchBookingsDts.getStartDate());
            LocalDate endDate = dateTimeMapper.dateDtoToDate(searchBookingsDts.getEndDate());
            LocalDateTime startDateTime = LocalDateTime.of(startDate, LocalTime.of(0, 0));
            LocalDateTime endDateTime = LocalDateTime.of(endDate, LocalTime.of(23, 59));
            return new MutablePair<>(startDateTime, endDateTime);
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public List<BookingDto> findBookingsByService(SearchBookingsDts searchBookingsDts) {
        Preconditions.checkNotNull(searchBookingsDts);
        if (searchBookingsDts.getServiceDto() != null) {
            Service service = servicesMapper.serviceDtoToService(searchBookingsDts.getServiceDto());
            MutablePair<LocalDateTime, LocalDateTime> timePair = mapSearchDatesToDateTimes(searchBookingsDts);
            List<Booking> bookings = bookingDao.findBookingByServiceAndTime(
                    service, timePair.getLeft(), timePair.getRight());
            List<Booking> sortedBookings = sortBookingsByTimeAsc(bookings);
            return bookingMapper.bookingsToBookingDtos(sortedBookings);
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public List<BookingDto> findFutureClientsBookings() {
        BasicUser currentUser = userService.findCurrentUser();
        if (currentUser != null) {
            List<Booking> bookings = bookingDao.findBookingByClientAndStartTimeAfter(currentUser, LocalDateTime.now());
            List<Booking> sortedBookings = sortBookingsByTimeAsc(bookings);
            return bookingMapper.bookingsToBookingDtos(sortedBookings);
        }
        throw new CorruptDataException();
    }

    @Override
    public void cancelBooking(CancelBookingDto cancelBookingDto) {
        Preconditions.checkNotNull(cancelBookingDto);

        if (cancelBookingDto.getBookingId() != null && cancelBookingDto.getNote() != null) {
            BasicUser currentUser = userService.findCurrentUser();
            if (currentUser != null) {
                if (currentUser.getRole() == Role.EMPLOYEE) {
                    cancelBookingEmployee(cancelBookingDto);
                }
                if (currentUser.getRole() == Role.CLIENT) {
                    cancelBookingClient(cancelBookingDto);
                }
                return;
            }
            throw new CorruptDataException();
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public List<BookingDto> findOldClientBookings() {
        BasicUser currentUser = userService.findCurrentUser();
        if (currentUser != null) {
            List<Booking> bookings = bookingDao.findBookingByClientAndStartTimeBefore(currentUser, LocalDateTime.now());
            List<Booking> sortedBookings = sortBookingsByTimeDesc(bookings);
            return bookingMapper.bookingsToBookingDtos(sortedBookings);
        }
        throw new CorruptDataException();
    }

    @Override
    public List<BookingDto> findCanceledBookingsByDay(SearchBookingsDts searchBookingsDts) {
        Preconditions.checkNotNull(searchBookingsDts);
        return findBookingsByDayWithStatus(searchBookingsDts, BookingState.CANCELLED);
    }

    @Override
    public List<BookingDto> findBookingsByCurrentEmployee(SearchBookingsDts searchBookingsDts) {
        Preconditions.checkNotNull(searchBookingsDts);
        BasicUser currentUser = userService.findCurrentUser();
        if(currentUser != null && currentUser.getRole() == Role.EMPLOYEE){
            return findBookingsByBasicUser(searchBookingsDts, currentUser);
        }
        throw new CorruptDataException();
    }

    private void cancelBookingClient(CancelBookingDto cancelBookingDto) {
        // TODO Implement - jezeli zajdzie taka potrzeba
    }

    private void cancelBookingEmployee(CancelBookingDto cancelBookingDto) {
        Booking found = bookingDao.findOne(cancelBookingDto.getBookingId());
        // TODO wyslij maila clientowi ze jego booking zostal skancelowany z odpowiednia notatka. Użyj AWS do tego ?
        if (found != null) {
            found.setBookingState(BookingState.CANCELLED);
            bookingDao.save(found);
            return;
        }
        throw new DataNotFoundException();
    }


    private boolean checkTimeOverlaping(LocalTime startTime, LocalTime endTime, LocalTime otherStartTime, LocalTime otherEndTime) {
        if (startTime.compareTo(otherStartTime) > 0 && startTime.compareTo(otherEndTime) < 0) {
            return true;
        }
        if (endTime.compareTo(otherStartTime) > 0 && endTime.compareTo(otherEndTime) < 0) {
            return true;
        }
        if (otherStartTime.compareTo(startTime) > 0 && otherEndTime.compareTo(endTime) < 0) {
            return true;
        }
        return false;
    }


}
