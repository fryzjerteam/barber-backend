package barber.bookingmanagement.service.impl;

import barber.bookingmanagement.dao.ScheduleDao;
import barber.bookingmanagement.dto.ScheduleDto;
import barber.bookingmanagement.entity.Schedule;
import barber.bookingmanagement.entity.ScheduleDay;
import barber.bookingmanagement.mapper.ScheduleMapper;
import barber.bookingmanagement.service.ScheduleService;
import barber.errorhandling.httpstatus.DataNotFoundException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.general.common.api.datatype.Role;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.dao.BasicUserDao;
import barber.usermanagement.entity.BasicUser;
import barber.usermanagement.mapper.UserMapper;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.util.List;

@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Autowired
    ScheduleDao scheduleDao;
    @Autowired
    UserMapper userMapper;
    @Autowired
    ScheduleMapper scheduleMapper;
    @Autowired
    BasicUserDao basicUserDao;

    @Override
    public void createEmptySchedule(BasicUser basicUser) {
        if(basicUser != null && basicUser.getRole() == Role.EMPLOYEE){
            Schedule schedule = new Schedule();
            schedule.setEmployee(basicUser);
            schedule.setMonday(getEmptyScheduleDay());
            schedule.setTuesday(getEmptyScheduleDay());
            schedule.setWednesday(getEmptyScheduleDay());
            schedule.setThursday(getEmptyScheduleDay());
            schedule.setFriday(getEmptyScheduleDay());
            schedule.setSaturday(getEmptyScheduleDay());
            schedule.setSunday(getEmptyScheduleDay());
            scheduleDao.save(schedule);
        }
    }

    private ScheduleDay getEmptyScheduleDay(){
        ScheduleDay day = new ScheduleDay();
        day.setStartTime(LocalTime.of(0,0));
        day.setEndTime(LocalTime.of(0,0));
        return day;
    }

    @Override
    public List<ScheduleDto> findScheduleByUser(UserDto userDto) {
        Preconditions.checkNotNull(userDto);

        if(userDto.getId() != null){
            List<Schedule> schedules = findScheduleByUser(userMapper.userDtotoBasicUser(userDto));
            return scheduleMapper.schedulesToSchedulesDto(schedules);
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public List<Schedule> findScheduleByUser(BasicUser basicUser) {
        Preconditions.checkNotNull(basicUser);
        return scheduleDao.findScheduleByEmployee(basicUser);
    }

    @Override
    public ScheduleDto addSchedule(ScheduleDto scheduleDto) {
        Preconditions.checkNotNull(scheduleDto);
        UserDto userDto = scheduleDto.getUserDto();

        if(userDto != null && userDto.getId() != null){
            BasicUser foundUser = basicUserDao.findOne(userDto.getId());
            if(foundUser != null){
                Schedule schedule = scheduleMapper.scheduleDtoToSchedule(scheduleDto);
                schedule.setEmployee(foundUser);

                Schedule savedSchedule = scheduleDao.save(schedule);
                return mapSavedSchedule(savedSchedule);
            }
            throw new DataNotFoundException();
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public ScheduleDto updateSchedule(ScheduleDto scheduleDto) {
        Preconditions.checkNotNull(scheduleDto);

        if(scheduleDto.getId() != null){
            Schedule foundSchedule = scheduleDao.findOne(scheduleDto.getId());
            scheduleMapper.updateScheduleFromDto(scheduleDto, foundSchedule);
            Schedule savedSchedule = scheduleDao.save(foundSchedule);
            return mapSavedSchedule(savedSchedule);
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public void deleteSchedule(String id) {
        Preconditions.checkNotNull(id);

        if(!id.isEmpty()){
            scheduleDao.delete(Long.parseLong(id));
        }
        throw new WrongDataReceivedException();
    }

    private ScheduleDto mapSavedSchedule(Schedule savedSchedule){
        ScheduleDto scheduleToReturn = scheduleMapper.scheduleToScheduleDto(savedSchedule);
        scheduleToReturn.setUserDto(userMapper.basicUserToUserDto(savedSchedule.getEmployee()));
        return scheduleToReturn;
    }
}
