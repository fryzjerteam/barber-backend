package barber.bookingmanagement.service;

import barber.bookingmanagement.dto.ServiceAssignmentDto;
import barber.bookingmanagement.dto.ServiceDto;
import barber.bookingmanagement.entity.Service;
import barber.bookingmanagement.entity.ServiceAssignment;
import barber.general.common.api.to.UserDto;

import java.util.List;

public interface ServiceAssignmentService {

    void deleteAssignment(ServiceAssignmentDto serviceAssignmentDto);

    List<ServiceDto> getServicesByUser(UserDto userDto);

    ServiceDto addAssignment(ServiceAssignmentDto serviceAssignmentDto);

    List<ServiceAssignment> findServiceAssignmentsByService(Service service);

    void deactivateAssignments(Service service);
}
