package barber.bookingmanagement.service;

import barber.bookingmanagement.dto.*;

import java.util.List;

public interface BookingService {

    List<FreeBookingDto> findFreeBookings(SearchFreeBookingDts searchFreeBookingDts);

    BookingDto createClientBooking(BookingDto bookingDto);

    List<BookingDto> findBookingsByEmployee(SearchBookingsDts searchBookingsDts);

    List<BookingDto> findBookingsByDay(SearchBookingsDts searchBookingsDts);

    List<BookingDto> findBookingsByService(SearchBookingsDts searchBookingsDts);

    List<BookingDto> findFutureClientsBookings();

    void cancelBooking(CancelBookingDto cancelBookingDto);

    List<BookingDto> findOldClientBookings();

    List<BookingDto> findCanceledBookingsByDay(SearchBookingsDts searchBookingsDts);

    List<BookingDto> findBookingsByCurrentEmployee(SearchBookingsDts searchBookingsDts);
}
