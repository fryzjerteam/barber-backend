package barber.bookingmanagement.service;

public interface ApplicationSettingsService {

    /**
     * Used for get global settings of the application.
     * @param setting This parameter can be taken from enum {@link barber.bookingmanagement.common.AppSettingsPath}
     * @return Requested setting. See {@link barber.bookingmanagement.common.AppSettingsPath} for a format of the output string.
     */
    String getSetting(String setting);
}
