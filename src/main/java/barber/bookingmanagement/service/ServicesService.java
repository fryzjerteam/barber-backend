package barber.bookingmanagement.service;

import barber.bookingmanagement.dto.ServiceDto;

import java.util.List;

public interface ServicesService {
    List<ServiceDto> getAllServices();
    List<ServiceDto> addService(ServiceDto serviceDto);

    ServiceDto updateService(ServiceDto serviceDto);

    void deleteService(String id);
}
