package barber.bookingmanagement.entity;

import barber.general.dataaccess.api.ApplicationPersistenceEntity;
import barber.usermanagement.entity.BasicUser;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetTime;

@Entity
@Table(name = "SERVICE_ASSIGNMENT")
public class ServiceAssignment extends ApplicationPersistenceEntity{

    @ManyToOne
    @JoinColumn(name = "SERVICE_ID")
    private Service service;

    @ManyToOne
    @JoinColumn(name = "BASIC_USER_ID")
    private BasicUser employee;

    private boolean deactivate;

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public BasicUser getEmployee() {
        return employee;
    }

    public void setEmployee(BasicUser employee) {
        this.employee = employee;
    }

    public boolean isDeactivate() {
        return deactivate;
    }

    public void setDeactivate(boolean deactivate) {
        this.deactivate = deactivate;
    }
}
