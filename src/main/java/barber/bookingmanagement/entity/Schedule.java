package barber.bookingmanagement.entity;

import barber.general.dataaccess.api.ApplicationPersistenceEntity;
import barber.usermanagement.entity.BasicUser;

import javax.persistence.*;

@Entity
@Table(name = "SCHEDULE")
public class Schedule extends ApplicationPersistenceEntity {

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE_ID")
    //@OnDelete(action = OnDeleteAction.CASCADE)
    private BasicUser employee;

    @Embedded
    private ScheduleDay monday;

    @Embedded
    private ScheduleDay tuesday;

    @Embedded
    private ScheduleDay wednesday;

    @Embedded
    private ScheduleDay thursday;

    @Embedded
    private ScheduleDay friday;

    @Embedded
    private ScheduleDay saturday;

    @Embedded
    private ScheduleDay sunday;


    public ScheduleDay getMonday() {
        return monday;
    }

    public void setMonday(ScheduleDay monday) {
        this.monday = monday;
    }

    public ScheduleDay getTuesday() {
        return tuesday;
    }

    public void setTuesday(ScheduleDay tuesday) {
        this.tuesday = tuesday;
    }

    public ScheduleDay getWednesday() {
        return wednesday;
    }

    public void setWednesday(ScheduleDay wednesday) {
        this.wednesday = wednesday;
    }

    public ScheduleDay getThursday() {
        return thursday;
    }

    public void setThursday(ScheduleDay thursday) {
        this.thursday = thursday;
    }

    public ScheduleDay getFriday() {
        return friday;
    }

    public void setFriday(ScheduleDay friday) {
        this.friday = friday;
    }

    public ScheduleDay getSaturday() {
        return saturday;
    }

    public void setSaturday(ScheduleDay saturday) {
        this.saturday = saturday;
    }

    public ScheduleDay getSunday() {
        return sunday;
    }

    public void setSunday(ScheduleDay sunday) {
        this.sunday = sunday;
    }

    public BasicUser getEmployee() {
        return employee;
    }

    public void setEmployee(BasicUser employee) {
        this.employee = employee;
    }
}
