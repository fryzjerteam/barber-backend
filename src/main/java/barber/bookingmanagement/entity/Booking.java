package barber.bookingmanagement.entity;

import barber.bookingmanagement.common.BookingState;
import barber.general.dataaccess.api.ApplicationPersistenceEntity;
import barber.usermanagement.entity.BasicUser;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "BOOKING")
public class Booking extends ApplicationPersistenceEntity{

    @ManyToOne
    @JoinColumn(name = "SERVICE_ASSIGNMENT_ID")
    private ServiceAssignment serviceAssignment;

    private LocalDateTime startTime;

    @ManyToOne
    @JoinColumn(name = "BASIC_USER_ID")
    private BasicUser client;

    private BookingState bookingState;

    public ServiceAssignment getServiceAssignment() {
        return serviceAssignment;
    }

    public void setServiceAssignment(ServiceAssignment serviceAssignment) {
        this.serviceAssignment = serviceAssignment;
    }

    public LocalDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalDateTime startTime) {
        this.startTime = startTime;
    }

    public BasicUser getClient() {
        return client;
    }

    public void setClient(BasicUser client) {
        this.client = client;
    }

    public BookingState getBookingState() {
        return bookingState;
    }

    public void setBookingState(BookingState bookingState) {
        this.bookingState = bookingState;
    }
}
