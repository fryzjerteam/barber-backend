package barber.bookingmanagement.entity;

import barber.general.dataaccess.api.ApplicationPersistenceEntity;
import barber.usermanagement.entity.BasicUser;

import javax.persistence.*;
import java.time.OffsetTime;

@Entity
@Table(name = "APPLICATION_SETTINGS")
public class ApplicationSettings extends ApplicationPersistenceEntity{

    private OffsetTime unitOffset;

    public OffsetTime getUnitOffset() {
        return unitOffset;
    }

    public void setUnitOffset(OffsetTime unitOffset) {
        this.unitOffset = unitOffset;
    }
}
