package barber.bookingmanagement.entity;

import barber.general.dataaccess.api.ApplicationPersistenceEntity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalTime;
import java.time.OffsetTime;

@Entity
@Table(name = "SERVICE")
public class Service extends ApplicationPersistenceEntity{

    private String name;
    private Integer durationUnits;
    private boolean deactivate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getDurationUnits() {
        return durationUnits;
    }

    public void setDurationUnits(Integer durationUnits) {
        this.durationUnits = durationUnits;
    }

    public boolean isDeactivate() {
        return deactivate;
    }

    public void setDeactivate(boolean deactivate) {
        this.deactivate = deactivate;
    }
}
