package barber.bookingmanagement.common;

public enum BookingState {
    ACTIVE,
    CANCELLED
}
