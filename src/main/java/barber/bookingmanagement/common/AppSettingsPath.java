package barber.bookingmanagement.common;

import java.util.HashSet;

public enum AppSettingsPath {

    // output in minutes
    UNIT_OFFSET("unitOffset");

    private String path;
    AppSettingsPath(String path){
        this.path = path;
        Static.paths.add(path);
    }

    public String getPath(){
        return this.path;
    }

    public static boolean contains(String path){
        return Static.paths.contains(path);
    }

    static private class Static{
        private static HashSet<String> paths = new HashSet<>();
    }
}
