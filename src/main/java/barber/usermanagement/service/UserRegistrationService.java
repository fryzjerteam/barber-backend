package barber.usermanagement.service;

import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.usermanagement.dto.*;
import barber.general.common.api.to.UserDto;

public interface UserRegistrationService {
    UserDto registerSimpleUser(BasicUserDto basicUserDto);
    UserDataDts registerUser(UserRegisterDataDts userRegisterDataDts);

    UserDataDts registerClient(UserRegisterDataDts userRegisterDataDts);

    PassChangeResponseDto changeUserPassword(PassChangeDto passChangeDto);
}
