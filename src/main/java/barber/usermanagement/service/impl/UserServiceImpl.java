package barber.usermanagement.service.impl;


import barber.bookingmanagement.service.ScheduleService;
import barber.bookingmanagement.service.ServiceAssignmentService;
import barber.errorhandling.httpstatus.CorruptDataException;
import barber.errorhandling.httpstatus.DataNotFoundException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.general.common.api.datatype.Role;
import barber.general.common.api.security.UserData;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.dao.BasicUserDao;
import barber.usermanagement.dto.UserDataDts;
import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.entity.BasicUser;
import barber.usermanagement.mapper.UserMapper;
import barber.usermanagement.service.UserDetailsService;
import barber.usermanagement.service.UserService;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.crypto.Data;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private static final String WILDCARD_CHAR = "*";
    public static final String SPACE_CHAR = " ";
    @Autowired
    private BasicUserDao userDao;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserDetailsService userDetailsService;
    @Autowired
    private ScheduleService scheduleService;
    @Autowired
    private ServiceAssignmentService serviceAssignmentService;

    @Override
    public void deleteUser(Long id) {
        Preconditions.checkNotNull(id);
        if(findCurrentUser().getId() == id){
            return;
        }
        BasicUser basicUser = userDao.findOne(id);
        if(basicUser != null){
            basicUser.setDeactivate(true);
            userDao.save(basicUser);
            userDetailsService.deleteUserDetailsByUserId(id);
            return;
        }
        throw new DataNotFoundException();
    }

    @Override
    public UserDataDts getUser(Long id) {
        Preconditions.checkNotNull(id);

        BasicUser foundUser = userDao.findOne(id);
        if (foundUser != null) {
            UserDetailsDto userDetailsDto = userDetailsService.findUserDetailsByUserId(id);
            if (userDetailsDto != null) {
                UserDataDts userDataDts = new UserDataDts();
                userDataDts.setUserDto(userMapper.basicUserToUserDto(foundUser));
                userDataDts.setUserDetailsDto(userDetailsDto);
                return userDataDts;
            }
        }
        throw new DataNotFoundException();
    }

    @Override
    public UserDataDts updateUser(UserDataDts userDataDts) {
        Preconditions.checkNotNull(userDataDts);

        UserDto userDto = userDataDts.getUserDto();
        BasicUser foundUser = userDao.findBasicUserByLogin(userDto.getName());
        if (foundUser != null) {
            foundUser.setFirstName(userDto.getFirstName());
            foundUser.setFirstName(userDto.getLastName());
            BasicUser updatedUser = userDao.save(foundUser);
            UserDetailsDto updatedUserDetailsDto = userDetailsService.updateUserDetailsByUser(
                    updatedUser, userDataDts.getUserDetailsDto());
            if (updatedUserDetailsDto != null) {
                UserDataDts updatedUserDataDts = new UserDataDts();
                updatedUserDataDts.setUserDto(userMapper.basicUserToUserDto(updatedUser));
                updatedUserDataDts.setUserDetailsDto(updatedUserDetailsDto);
                return updatedUserDataDts;
            } else {
                throw new CorruptDataException();
            }
        }
        throw new DataNotFoundException();
    }


    @Override
    public List<UserDto> findUsersByName(String name) {
        Preconditions.checkNotNull(name);
        return findUsersByNameAndRole(name, null);
    }

    private List<UserDto> findUsersByNameAndRole(String name, Role role) {
        if (!name.isEmpty()) {
            List<BasicUser> foundBasicUsers;
            if (WILDCARD_CHAR.equals(name)) {
                name = StringUtils.EMPTY;
                foundBasicUsers = findBasicUserByName(name, role);
            } else if (name.contains(SPACE_CHAR) && !name.endsWith(SPACE_CHAR) && !name.startsWith(SPACE_CHAR)) {
                String[] strings = name.split(SPACE_CHAR);
                foundBasicUsers = findBasicUserByNames(strings[0], strings[1], role);
            } else {
                foundBasicUsers = findBasicUserByName(name, role);
            }
            return userMapper.basicUsersToUserDtos(foundBasicUsers);
        }
        throw new WrongDataReceivedException();
    }

    private List<BasicUser> findBasicUserByNames(String firstName, String lastName, Role role) {
        List<BasicUser> foundBasicUsers;
        if (role == null) {
            foundBasicUsers = userDao.findBasicUserByNames(firstName, lastName);
        } else {
            foundBasicUsers = userDao.findBasicUserByNamesAndRole(firstName, lastName, role);
        }
        return foundBasicUsers;
    }

    private List<BasicUser> findBasicUserByName(String name, Role role) {
        List<BasicUser> foundBasicUsers;
        if (role == null) {
            foundBasicUsers = userDao.findBasicUserByName(name);
        } else {
            foundBasicUsers = userDao.findBasicUserByNameAndRole(name, role);
        }
        return foundBasicUsers;
    }


    @Override
    public List<UserDto> findUserByRole(String role) {
        Preconditions.checkNotNull(role);

        Role roleEnum = getRoleEnum(role);
        if (roleEnum != null) {
            List<BasicUser> foundBasicUsers = userDao.findBasicUserByRole(roleEnum);
            return userMapper.basicUsersToUserDtos(foundBasicUsers);
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public BasicUser findUserByLogin(String login) {
        Preconditions.checkNotNull(login);
        return userDao.findBasicUserByLogin(login);
    }

    @Override
    public List<UserDto> findClientsByName(String name) {
        Preconditions.checkNotNull(name);
        return findUsersByNameAndRole(name, Role.CLIENT);
    }

    @Override
    public BasicUser findCurrentUser() {
        UserData userData = UserData.get();
        if (userData != null && userData.getUserProfile() != null) {
            return findUserByLogin(userData.getUserProfile().getName());
        }
        return null;
    }


    private Role getRoleEnum(String role) {
        if (!role.isEmpty()) {
            for (Role roleEnum : Role.values()) {
                if (roleEnum.getName().equals(role)) {
                    return roleEnum;
                }
            }
        }
        return null;
    }
}
