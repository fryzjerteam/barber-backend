package barber.usermanagement.service.impl;

import barber.errorhandling.httpstatus.DataNotFoundException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.usermanagement.dao.UserDetailsDao;
import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.entity.BasicUser;
import barber.usermanagement.entity.UserDetails;
import barber.usermanagement.mapper.UserDetailsMapper;
import barber.usermanagement.service.UserDetailsService;
import barber.usermanagement.service.UserService;
import com.google.common.base.Preconditions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDetailsDao userDetailsDao;
    @Autowired
    private UserDetailsMapper userDetailsMapper;
    @Autowired
    private UserService userService;

    @Override
    public UserDetailsDto findUserDetailsByUserId(Long userId) {
        Preconditions.checkNotNull(userId);

        UserDetails userDetails = userDetailsDao.findUserDetailsByUserId(userId);
        if(userDetails != null){
            return userDetailsMapper.userDetailsToUserDetailsDto(userDetails);
        }
        throw new DataNotFoundException();
    }

    @Override
    public UserDetailsDto updateUserDetailsByUser(BasicUser basicUser, UserDetailsDto userDetailsDto) {
        Preconditions.checkNotNull(basicUser);
        Preconditions.checkNotNull(userDetailsDto);

        UserDetails foundUserDetails = userDetailsDao.findUserDetailsByUserId(basicUser.getId());
        if(foundUserDetails != null){
            foundUserDetails.setAddress(userDetailsDto.getAddress());
            foundUserDetails.setEmail(userDetailsDto.getEmail());
            foundUserDetails.setPhone(userDetailsDto.getPhone());
            foundUserDetails.setBasicUser(basicUser);
            UserDetails savedUserDetails = userDetailsDao.save(foundUserDetails);
            return userDetailsMapper.userDetailsToUserDetailsDto(savedUserDetails);
        }
        throw new DataNotFoundException();
    }

    @Override
    public void deleteUserDetailsByUserId(Long userId) {
        UserDetails foundUserDetails = userDetailsDao.findUserDetailsByUserId(userId);
        if(foundUserDetails != null){
            userDetailsDao.delete(foundUserDetails.getId());
        }
    }

    @Override
    public UserDetailsDto createUserDetails(UserDetailsDto userDetailsDto) {
        Preconditions.checkNotNull(userDetailsDto);

        if(userDetailsDto.getUserId() != null){
            UserDetails foundUserDetails = userDetailsDao.findUserDetailsByUserId(userDetailsDto.getUserId());
            if(foundUserDetails != null){
                UserDetails userDetails = userDetailsMapper.userDetailsDtoToUserDetails(userDetailsDto);
                UserDetails savedUserDetails = userDetailsDao.save(userDetails);
                return userDetailsMapper.userDetailsToUserDetailsDto(savedUserDetails);
            }
            else{
                userDetailsMapper.updateUserDetailsFromDto(userDetailsDto, foundUserDetails);
                UserDetails updatedUserDetails = userDetailsDao.save(foundUserDetails);
                return userDetailsMapper.userDetailsToUserDetailsDto(updatedUserDetails);
            }
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public UserDetailsDto findUserDetailsByUserId(String userId) {
        Preconditions.checkNotNull(userId);

        if(!userId.isEmpty()){
            return findUserDetailsByUserId(Long.parseLong(userId));
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public UserDetailsDto getCurrentUserDetails() {
        BasicUser currentUser = userService.findCurrentUser();
        return findUserDetailsByUserId(currentUser.getId());
    }

    @Override
    public UserDetailsDto updateCurrentUserDetails(UserDetailsDto userDetailsDto) {
        Preconditions.checkNotNull(userDetailsDto);

        BasicUser currentUser = userService.findCurrentUser();
        UserDetails userDetails = userDetailsDao.findUserDetailsByUserId(currentUser.getId());
        if(userDetails != null){
            userDetailsMapper.updateUserDetailsFromDto(userDetailsDto, userDetails);
            return userDetailsMapper.userDetailsToUserDetailsDto(userDetails);
        }
        throw new DataNotFoundException();
    }
}
