package barber.usermanagement.service.impl;

import barber.aws.AwsEmailService;
import barber.bookingmanagement.service.ScheduleService;
import barber.errorhandling.httpstatus.CorruptDataException;
import barber.errorhandling.httpstatus.WrongDataReceivedException;
import barber.general.common.api.datatype.Role;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.dao.BasicUserDao;
import barber.usermanagement.dao.UserDetailsDao;
import barber.usermanagement.dto.*;
import barber.usermanagement.entity.BasicUser;
import barber.usermanagement.entity.UserDetails;
import barber.usermanagement.mapper.UserDetailsMapper;
import barber.usermanagement.mapper.UserMapper;
import barber.usermanagement.service.UserRegistrationService;
import barber.usermanagement.service.UserService;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
public class UserRegistrationServiceImpl implements UserRegistrationService {

    public static final int PASSWORD_COUNT = 8;
    public static final String FIRST_CLIENT_SUFIX = "0000";
    public static final int PASS_MIN_SIZE = 8;
    @Autowired
    private BasicUserDao basicUserDao;
    @Autowired
    private UserDetailsDao userDetailsDao;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private UserDetailsMapper userDetailsMapper;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    ScheduleService scheduleService;
    @Autowired
    UserService userService;
    @Autowired
    AwsEmailService awsEmailService;

    @Override
    public UserDto registerSimpleUser(BasicUserDto basicUserDto) {
        return userMapper.basicUserToUserDto(registerBasicUser(basicUserDto));
    }

    @Override
    public UserDataDts registerUser(UserRegisterDataDts userRegisterDataDts) {
        if (checkRegisterPreconditions(userRegisterDataDts)) {
            return registerUserWithDetails(userRegisterDataDts);
        }
        throw new  WrongDataReceivedException();
    }

    private UserDataDts registerUserWithDetails(UserRegisterDataDts userRegisterDataDts) {
        BasicUserDto basicUserDto = userRegisterDataDts.getBasicUserDto();
        UserDetailsDto userDetailsDto = userRegisterDataDts.getUserDetailsDto();

        BasicUser registeredUser = registerBasicUser(basicUserDto);
        UserDataDts userDataToReturn = null;

        if (registeredUser != null) {
            UserDetails userDetails = userDetailsMapper.userDetailsDtoToUserDetails(userDetailsDto);
            userDetails.setBasicUser(registeredUser);
            UserDetailsDto createdUserDetails = userDetailsMapper.userDetailsToUserDetailsDto(userDetailsDao.save(userDetails));

            userDataToReturn = new UserDataDts();
            userDataToReturn.setUserDto(userMapper.basicUserToUserDto(registeredUser));
            userDataToReturn.setUserDetailsDto(createdUserDetails);

            scheduleService.createEmptySchedule(registeredUser);
        }
        return userDataToReturn;
    }

    private boolean checkRegisterPreconditions(UserRegisterDataDts userRegisterDataDts) {
        Preconditions.checkNotNull(userRegisterDataDts);
        if (userRegisterDataDts.getBasicUserDto() != null && userRegisterDataDts.getUserDetailsDto() != null) {
            return true;
        }
        return false;
    }


    @Override
    public UserDataDts registerClient(UserRegisterDataDts userRegisterDataDts) {
        if(checkRegisterPreconditions(userRegisterDataDts)){
            BasicUserDto basicUserDto = userRegisterDataDts.getBasicUserDto();
            String firstName = basicUserDto.getFirstName();
            String lastName = basicUserDto.getLastName();
            if(firstName != null && lastName != null && firstName.length() >= 3 && lastName.length() >= 3){
                String login = createLogin(firstName, lastName);
                if(login != null ){
                    String password = RandomStringUtils.randomAscii(PASSWORD_COUNT);
                    basicUserDto.setName(login);
                    basicUserDto.setPassword(password);
                    basicUserDto.setRole(Role.CLIENT);
                    awsEmailService.sendRegistrationEmail(userRegisterDataDts);
                    return registerUserWithDetails(userRegisterDataDts);
                }
                throw new CorruptDataException();
            }
        }
        throw new WrongDataReceivedException();
    }

    @Override
    public PassChangeResponseDto changeUserPassword(PassChangeDto passChangeDto) {
        Preconditions.checkNotNull(passChangeDto);

        String oldPass = passChangeDto.getOldPass();
        String newPass1 = passChangeDto.getNewPass1();
        String newPass2 = passChangeDto.getNewPass2();
        if(checkPassPreconditions(passChangeDto)){
            if(!newPass1.equals(newPass2)){
                return PassChangeResponseDto.error(PassChangeResponseDto.MSG_DIFF_NEW_PASSES);
            }
            if(newPass1.length() < PASS_MIN_SIZE){
                return PassChangeResponseDto.error(PassChangeResponseDto.MSG_PASS_TOO_SHORT);
            }
            BasicUser currentUser = userService.findCurrentUser();
            if(currentUser != null){
                if(!passwordEncoder.matches(oldPass, currentUser.getPassword())){
                    return PassChangeResponseDto.error(PassChangeResponseDto.MSG_WRONG_OLD_PASS);
                }
                if(oldPass.equals(newPass1)){
                    return PassChangeResponseDto.error(PassChangeResponseDto.MSG_THE_SAME_PASSES);
                }
                currentUser.setPassword(passwordEncoder.encode(newPass1));
                basicUserDao.save(currentUser);
                return PassChangeResponseDto.ok();
            }
            throw new CorruptDataException();
        }
        throw new WrongDataReceivedException();
    }

    boolean checkPassPreconditions(PassChangeDto passChangeDto){
        String oldPass = passChangeDto.getOldPass();
        String newPass1 = passChangeDto.getNewPass1();
        String newPass2 = passChangeDto.getNewPass2();
        if(oldPass != null && newPass1 != null && newPass2 != null && !oldPass.isEmpty() && !newPass1.isEmpty() && !newPass2.isEmpty()){
            return true;
        }
        return false;
    }

    private String createLogin(String firstName, String lastName) {
        String login = firstName.substring(0,3).concat(lastName.substring(0,3)).toLowerCase();
        BasicUser topByNameDesc = basicUserDao.findTopByNameStartingWithOrderByNameDesc(login);
        if(topByNameDesc == null){
            login = login.concat(FIRST_CLIENT_SUFIX);
        }
        else{
            String sufix = topByNameDesc.getName().substring(6);
            sufix = String.valueOf(Integer.parseInt(sufix) + 1);
            sufix = StringUtils.leftPad(sufix, 4, '0');
            login = login.concat(sufix);
        }
        return login;
    }

    private BasicUser registerBasicUser(BasicUserDto basicUserDto) {
        String login = basicUserDto.getName();
        if (login != null) {
            BasicUser foundBasicUser = basicUserDao.findBasicUserByLogin(login);
            if (foundBasicUser == null) {
                BasicUser basicUser = userMapper.basicUserDtoToBasicUser(basicUserDto);
                // encrypting the password
                basicUser.setPassword(passwordEncoder.encode(basicUserDto.getPassword()));
                return basicUserDao.save(basicUser);
            }
            return null;
        }
        throw new WrongDataReceivedException();
    }
}
