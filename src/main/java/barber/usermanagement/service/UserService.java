package barber.usermanagement.service;



import barber.general.common.api.to.UserDto;
import barber.usermanagement.dto.UserDataDts;
import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.entity.BasicUser;

import java.util.List;

public interface UserService {

    void deleteUser(Long id);
    UserDataDts updateUser(UserDataDts userDataDts);
    UserDataDts getUser(Long id);
    List<UserDto> findUsersByName(String name);
    List<UserDto> findUserByRole(String role);

    BasicUser findUserByLogin(String login);

    List<UserDto> findClientsByName(String name);
    BasicUser findCurrentUser();
}
