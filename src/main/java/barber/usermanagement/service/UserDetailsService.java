package barber.usermanagement.service;

import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.entity.BasicUser;

public interface UserDetailsService {
    UserDetailsDto findUserDetailsByUserId(Long userId);

    UserDetailsDto updateUserDetailsByUser(BasicUser basicUser, UserDetailsDto userDetailsDto);

    void deleteUserDetailsByUserId(Long userId);

    UserDetailsDto createUserDetails(UserDetailsDto userDetailsDto);

    UserDetailsDto findUserDetailsByUserId(String userId);

    UserDetailsDto getCurrentUserDetails();

    UserDetailsDto updateCurrentUserDetails(UserDetailsDto userDetailsDto);
}
