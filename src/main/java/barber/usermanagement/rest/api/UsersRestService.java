package barber.usermanagement.rest.api;

import barber.general.common.api.RestService;
import barber.general.common.api.to.UserDto;
import barber.usermanagement.dto.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;


@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public interface UsersRestService extends RestService {

  @POST
  @Path("/user")
  UserDataDts registerUser(UserRegisterDataDts userRegisterDataDts);

  @POST
  @Path("/registerClient")
  UserDataDts registerClient(UserRegisterDataDts userRegisterDataDts);

  @GET
  @Path("/user")
  UserDataDts getUser(@QueryParam("id") String id);

  @PUT
  @Path("/user")
  UserDataDts updateUser(UserDataDts userDataDts);

  @DELETE
  @Path("/user")
  void deleteUser(@QueryParam("id") String id);


  @GET
  @Path("/findUsersByName")
  List<UserDto> findUsersByName(@QueryParam("name") String name);

  @GET
  @Path("/findClientsByName")
  List<UserDto> findClientsByName(@QueryParam("name") String name);

  @GET
  @Path("/findUsersByRole")
  List<UserDto> findUsersByRole(@QueryParam("role") String role);

  @POST
  @Path("/userDetails")
  UserDetailsDto createUserDetails(UserDetailsDto userDetailsDto);

  @GET
  @Path("/userDetails")
  UserDetailsDto getUserDetails(@QueryParam("userId") String userId);

  @GET
  @Path("/currentUserDetails")
  UserDetailsDto getCurrentUserDetails();

  @PUT
  @Path("/currentUserDetails")
  UserDetailsDto updateCurrentUserDetails(UserDetailsDto userDetailsDto);

  @POST
  @Path("/changePass")
  PassChangeResponseDto changeUserPassword(PassChangeDto passChangeDto);

}
