package barber.usermanagement.rest.impl;

import barber.general.common.api.to.UserDto;
import barber.usermanagement.dto.*;
import barber.usermanagement.rest.api.UsersRestService;
import barber.usermanagement.service.UserDetailsService;
import barber.usermanagement.service.UserRegistrationService;
import barber.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import javax.transaction.Transactional;
import java.util.List;

@Named
@Transactional
public class UsersRestServiceImpl implements UsersRestService {

    @Autowired
    private UserRegistrationService userRegistrationService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDetailsService userDetailsService;


    @Override
    public UserDataDts registerUser(UserRegisterDataDts userRegisterDataDts){
        return userRegistrationService.registerUser(userRegisterDataDts);
    }

    @Override
    public UserDataDts registerClient(UserRegisterDataDts userRegisterDataDts) {
        return userRegistrationService.registerClient(userRegisterDataDts);
    }

    @Override
    public UserDataDts getUser(String id) {
        return userService.getUser(Long.parseLong(id));
    }

    @Override
    public UserDataDts updateUser(UserDataDts userDataDts) {
        return userService.updateUser(userDataDts);
    }

    @Override
    public void deleteUser(String id) {
        userService.deleteUser(Long.parseLong(id));
    }

    @Override
    public List<UserDto> findUsersByName(String name) {
        return userService.findUsersByName(name);
    }

    @Override
    public List<UserDto> findClientsByName(String name) {
        return userService.findClientsByName(name);
    }

    @Override
    public List<UserDto> findUsersByRole(String role) {
        return userService.findUserByRole(role);
    }

    @Override
    public UserDetailsDto createUserDetails(UserDetailsDto userDetailsDto) {
        return userDetailsService.createUserDetails(userDetailsDto);
    }

    @Override
    public UserDetailsDto getUserDetails(String userId) {
        return userDetailsService.findUserDetailsByUserId(userId);
    }

    @Override
    public UserDetailsDto getCurrentUserDetails() {
        return userDetailsService.getCurrentUserDetails();
    }

    @Override
    public UserDetailsDto updateCurrentUserDetails(UserDetailsDto userDetailsDto) {
        return userDetailsService.updateCurrentUserDetails(userDetailsDto);
    }

    @Override
    public PassChangeResponseDto changeUserPassword(PassChangeDto passChangeDto) {
        return userRegistrationService.changeUserPassword(passChangeDto);
    }
}
