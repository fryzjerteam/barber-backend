package barber.usermanagement.dao;

import barber.general.common.api.datatype.Role;
import barber.general.dataaccess.api.dao.ApplicationDao;
import barber.usermanagement.entity.BasicUser;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BasicUserDao extends ApplicationDao<BasicUser> {

    @Query("SELECT u FROM BasicUser u WHERE u.name=:login and u.deactivate=false")
    BasicUser findBasicUserByLogin(@Param("login")String login);

    @Query("SELECT u FROM BasicUser u WHERE u.firstName LIKE %:firstName% AND u.lastName LIKE %:lastName% and u.deactivate=false")
    List<BasicUser> findBasicUserByNames(@Param("firstName") String firstName, @Param("lastName") String lastName);

    @Query("SELECT u FROM BasicUser u WHERE (u.firstName LIKE %:name% OR u.lastName LIKE %:name%) and u.deactivate=false")
    List<BasicUser> findBasicUserByName(@Param("name") String name);

    @Query("SELECT u FROM BasicUser u WHERE u.role=:role and u.deactivate=false")
    List<BasicUser> findBasicUserByRole(@Param("role") Role role);

    @Query("SELECT u FROM BasicUser u WHERE u.firstName LIKE %:first% AND u.lastName LIKE %:last% AND u.role=:role and u.deactivate=false")
    List<BasicUser> findBasicUserByNamesAndRole(@Param("first") String firstName, @Param("last") String lastName, @Param("role") Role role);

    @Query("SELECT u FROM BasicUser u WHERE (u.firstName LIKE %:name% OR u.lastName LIKE %:name%) AND u.role=:role and u.deactivate=false")
    List<BasicUser> findBasicUserByNameAndRole(@Param("name") String name, @Param("role") Role role);

    BasicUser findTopByNameStartingWithOrderByNameDesc(@Param("name") String name);

    @Query("select u from BasicUser u where u.id=:id and u.deactivate=false")
    BasicUser findOne(@Param("id") Long id);
}
