package barber.usermanagement.dao;

import barber.general.dataaccess.api.dao.ApplicationDao;
import barber.usermanagement.entity.UserDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface UserDetailsDao extends ApplicationDao<UserDetails> {

    @Query(value = "SELECT d FROM UserDetails d WHERE d.basicUser.id=:userId")
    UserDetails findUserDetailsByUserId(@Param("userId")Long userId);
}
