package barber.usermanagement.mapper;

import barber.usermanagement.dto.UserDetailsDto;
import barber.usermanagement.entity.UserDetails;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring")
public interface UserDetailsMapper {


    @Mapping(source = "userId", target = "basicUser.id")
    @Mapping(ignore = true, target = "id")
    @Mapping(ignore = true, target = "modificationCounter")
    @Mapping(ignore = true, target = "revision")
    UserDetails userDetailsDtoToUserDetails(UserDetailsDto userDetailsDto);

    @Mapping(source = "basicUser.id", target = "userId")
    UserDetailsDto userDetailsToUserDetailsDto(UserDetails userDetails);

    @Mapping(ignore = true, target = "id")
    @Mapping(ignore = true, target = "modificationCounter")
    @Mapping(ignore = true, target = "revision")
    @Mapping(ignore = true, target = "basicUser")
    void updateUserDetailsFromDto(UserDetailsDto userDetailsDto, @MappingTarget UserDetails userDetails);
}
