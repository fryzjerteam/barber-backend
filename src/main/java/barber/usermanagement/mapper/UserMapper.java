package barber.usermanagement.mapper;


import barber.usermanagement.dto.BasicUserDto;
import barber.usermanagement.entity.BasicUser;
import barber.general.common.api.to.UserDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public interface UserMapper {

    @Mapping(ignore = true, target = "password")
    @Mapping(ignore = true, target = "modificationCounter")
    @Mapping(ignore = true, target = "revision")
    @Mapping(ignore = true, target = "deactivate")
    BasicUser basicUserDtoToBasicUser(BasicUserDto basicUserDto);

    BasicUserDto basicUserToBasicUserDto(BasicUser basicUser);

    UserDto basicUserToUserDto(BasicUser basicUser);

    @Mapping(ignore = true, target = "password")
    @Mapping(ignore = true, target = "modificationCounter")
    @Mapping(ignore = true, target = "revision")
    @Mapping(ignore = true, target = "deactivate")
    BasicUser userDtotoBasicUser(UserDto userDto);

    default List<UserDto> basicUsersToUserDtos(List<BasicUser> basicUsers){
        return basicUsers.stream().map(user -> basicUserToUserDto(user)).collect(Collectors.toList());
    }
}
