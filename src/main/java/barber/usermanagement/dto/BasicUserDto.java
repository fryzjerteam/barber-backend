package barber.usermanagement.dto;

import barber.general.common.api.to.UserDto;


public class BasicUserDto extends UserDto {

    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
