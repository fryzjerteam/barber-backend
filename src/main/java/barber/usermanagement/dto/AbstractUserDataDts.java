package barber.usermanagement.dto;

public abstract class AbstractUserDataDts {
    private UserDetailsDto userDetailsDto;

    public UserDetailsDto getUserDetailsDto() {
        return userDetailsDto;
    }

    public void setUserDetailsDto(UserDetailsDto userDetailsDto) {
        this.userDetailsDto = userDetailsDto;
    }
}
