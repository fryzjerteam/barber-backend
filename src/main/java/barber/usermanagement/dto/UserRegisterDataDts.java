package barber.usermanagement.dto;

import dz.jtsgen.annotations.TypeScript;

@TypeScript
public class UserRegisterDataDts extends AbstractUserDataDts {
    private BasicUserDto basicUserDto;

    public BasicUserDto getBasicUserDto() {
        return basicUserDto;
    }

    public void setBasicUserDto(BasicUserDto basicUserDto) {
        this.basicUserDto = basicUserDto;
    }


}
