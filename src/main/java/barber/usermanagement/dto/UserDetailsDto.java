package barber.usermanagement.dto;


import io.oasp.module.basic.common.api.to.AbstractTo;

public class UserDetailsDto extends AbstractTo {
    private String address;
    private String phone;
    private String email;
    private Long UserId;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getUserId() {
        return UserId;
    }

    public void setUserId(Long userId) {
        UserId = userId;
    }
}
