package barber.usermanagement.dto;


import io.oasp.module.basic.common.api.to.AbstractTo;

public class PassChangeResponseDto extends AbstractTo {

    private static final int TYPE_ERR = 0;
    private static final int TYPE_OK = 1;

    public static final int MSG_WRONG_OLD_PASS = 0;
    public static final int MSG_DIFF_NEW_PASSES = 1;
    public static final int MSG_THE_SAME_PASSES = 2;
    public static final int MSG_PASS_TOO_SHORT = 3;
    private static final int MSG_OK = 4;

    private String type;
    private String message;

    private PassChangeResponseDto(){

    }

    private PassChangeResponseDto(int type, int message){
        switch (type){
            case TYPE_ERR:
                this.type = "ERR";
                break;
            case TYPE_OK:
                this.type = "OK";
                break;
        }
        switch(message){
            case MSG_WRONG_OLD_PASS:
                this.message = "The old password is incorrect!";
                break;
            case MSG_DIFF_NEW_PASSES:
                this.message = "New passwords must be the same!";
                break;
            case MSG_THE_SAME_PASSES:
                this.message = "New password and old password are the same!";
                break;
            case MSG_PASS_TOO_SHORT:
                this.message = "The password is too short!";
            case MSG_OK:
                this.message = "OK";
        }
    }

    public static PassChangeResponseDto error(int message){
        return new PassChangeResponseDto(TYPE_ERR, message);
    }

    public static PassChangeResponseDto ok(){
        return new PassChangeResponseDto(TYPE_OK, MSG_OK);
    }

    public String getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }
}
