package barber.usermanagement.dto;

import barber.general.common.api.to.UserDto;
import dz.jtsgen.annotations.TypeScript;

@TypeScript
public class UserDataDts extends AbstractUserDataDts {

    private UserDto userDto;


    public UserDto getUserDto() {
        return userDto;
    }

    public void setUserDto(UserDto userDto) {
        this.userDto = userDto;
    }
}
