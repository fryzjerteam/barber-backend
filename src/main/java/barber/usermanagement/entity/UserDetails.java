package barber.usermanagement.entity;

import barber.general.dataaccess.api.ApplicationPersistenceEntity;

import javax.persistence.*;

@Entity
@Table(name = "USER_DETAILS")
public class UserDetails extends ApplicationPersistenceEntity{

    private String address;
    private String phone;
    private String email;

    @OneToOne
    @JoinColumn(name = "BASIC_USER_ID", referencedColumnName="id")
    private BasicUser basicUser;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public BasicUser getBasicUser() {
        return basicUser;
    }

    public void setBasicUser(BasicUser basicUser) {
        this.basicUser = basicUser;
    }
}
