package barber.usermanagement.entity;

import barber.general.common.api.UserProfile;
import barber.general.common.api.datatype.Role;
import barber.general.dataaccess.api.ApplicationPersistenceEntity;
import org.hibernate.validator.constraints.Length;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "BASIC_USER")
public class BasicUser extends ApplicationPersistenceEntity implements UserProfile {

    private static final long serialVersionUID = 1L;

    @Column(name = "login", unique = true)
    private String name;

    private String firstName;

    private String lastName;

    @Length(min=60)
    private String password;

    private Role role;

    private boolean deactivate;

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getFirstName() {
        return firstName;
    }

    @Override
    public String getLastName() {
        return lastName;
    }

    @Override
    public Role getRole() {
        return role;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isDeactivate() {
        return deactivate;
    }

    public void setDeactivate(boolean deactivate) {
        this.deactivate = deactivate;
    }
}
