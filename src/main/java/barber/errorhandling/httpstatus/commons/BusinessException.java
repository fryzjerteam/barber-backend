package barber.errorhandling.httpstatus.commons;

import barber.general.common.api.NlsBundleApplicationRoot;
import barber.general.common.api.exception.ApplicationBusinessException;

public class BusinessException extends ApplicationBusinessException {
    protected static final String DATA_NOT_FOUND = "Cannot find some data on the database";
    protected static final String WRONG_DATA_RECEIVED = "The data received by a server is wrong";
    protected static final String CORRUPT_DATA = "Some of the processing data is corrupted";
    protected static final String USER_NOT_FOUND = "Cannot find the user";

    public BusinessException(String message) {
        this(null, message);
    }

    BusinessException(Throwable cause, String message) {
        super(cause, createBundle(NlsBundleApplicationRoot.class).errorSimpleMessage(message));
    }
}
