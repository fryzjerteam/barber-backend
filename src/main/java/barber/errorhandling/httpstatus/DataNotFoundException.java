package barber.errorhandling.httpstatus;

import barber.errorhandling.httpstatus.commons.BusinessException;

public class DataNotFoundException extends BusinessException {

    public DataNotFoundException() {
        super(BusinessException.DATA_NOT_FOUND);
    }
}
