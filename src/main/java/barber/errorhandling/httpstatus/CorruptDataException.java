package barber.errorhandling.httpstatus;

import barber.errorhandling.httpstatus.commons.BusinessException;

public class CorruptDataException extends BusinessException {

    public CorruptDataException() {
        super(BusinessException.CORRUPT_DATA);
    }
}
