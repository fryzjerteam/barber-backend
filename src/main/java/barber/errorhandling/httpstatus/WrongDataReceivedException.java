package barber.errorhandling.httpstatus;

import barber.errorhandling.httpstatus.commons.BusinessException;

public class WrongDataReceivedException extends BusinessException {
    public WrongDataReceivedException() {
        super(BusinessException.WRONG_DATA_RECEIVED);
    }
}
