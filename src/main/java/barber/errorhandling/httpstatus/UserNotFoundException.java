package barber.errorhandling.httpstatus;

import barber.errorhandling.httpstatus.commons.BusinessException;

public class UserNotFoundException extends BusinessException {
    public UserNotFoundException() {
        super(BusinessException.USER_NOT_FOUND);
    }
}
